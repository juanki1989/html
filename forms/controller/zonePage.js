var idZone = null;
var idSensor = null;

$(document).ready(function(){
    if(readCookie('admin') == "admin")
        ShowAdminLayout();
    else
        window.location.href = "loginAdmin.html"; 
    idZone = readCookie('idZone');
    completeZone(idZone);
    completeArraySensors(idZone);
})

//complete zone form
function completeZone(idZone)
{
	var zone = getZone(idZone);
	document.getElementById("name").value = zone[1];
}

//get zone from BD
function getZone(idZone)
{
	var parametros = {
					"idZone": idZone,
					"function" : "getZone"
				};
				
	var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/zone.php',
        'data': parametros,
        beforeSend: function(){
		},
        'success': function (request) {
        	tmp = eval(request); 	
        }
    });
    return tmp;
	}();
	return return_first;
}

//save zone 
function saveZone()
{
    var name = document.getElementById("name").value;
    if(idZone != null)
    {
        if(name != ""){
            updateZone(name);
            putClassInDiv("divNameZone", "form-group");
            putMessageInElementSucces("message", "div", "alert-success form-group", "Updated");
        }else
            putMessageInElement("message", "divNameStore", "The name is empty", "has-error form-group");   
    }
        
}

function updateZone(name)
{
    var parametros = {
        "name": name,
        "idZone": idZone,
        "function": "updateZone"
    };
    $.ajax({
        data: parametros,
        url: '../model/zone.php',
        type: 'post',
        async: false,
        beforeSend: function(){
        },
        success: function(response){
            
        },
        error: function(jqXHR, estado, error){
            console.log(estado)
            console.log(error)
        }
    });

}

//complete array sensors
function completeArraySensors(idZone)
{
    var sensors = getSensorsArray(idZone);
    for(var i = 0; i < sensors.length; i++)
    {
        insertItemInTableSensor(sensors[i]);
    }
}

//get sensor aray from BD
function getSensorsArray(idZone)
{
    var parametros = {
                    "idZone": idZone
                };
                
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/sensors.php',
        'data': parametros,
        beforeSend: function(){
        
        },
        'success': function (request) {
            tmp = eval(request);
         
        }
    });
    return tmp;
    }();
    return return_first;
}

//insert item in table sensors
function insertItemInTableSensor(sensor)
{
    var table = document.getElementById("idTableSensors");
    var row = table.insertRow(1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2)
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var idZone = "'"+sensor[0]+"'";
    cell1.innerHTML = '<a href="#myModal" role="button" class="alert-link" data-toggle="modal" onclick="getSensor('+idZone+')">'+sensor[0]+"</a>";
    cell2.innerHTML = '<a href="#myModal" role="button" class="alert-link" data-toggle="modal" onclick="getSensor('+idZone+')">'+sensor[1]+"</a>";
    cell3.innerHTML = sensor[2];
    cell4.innerHTML = sensor[3];
    cell5.innerHTML = sensor[4]; 
    cell6.innerHTML = '<button type="submit" class="btn btn-danger" onclick="deleteSensor('+idZone+')">Delete</button>';     
}

// save sensor in BD
function saveSensor()
{
    var mac = document.getElementById("macSensor").value;
    var name = document.getElementById("nameSensor").value;
    var model = document.getElementById("model").value;
    var type = document.getElementById("typeSensor").value;
    var ip = document.getElementById("ip").value;   
    var sensorReadSampleTime = document.getElementById("sensorReadSampleTime").value;
    var sensorProgramStartTime = document.getElementById("sensorProgramStartTime").value;
    var sensorProgramEndTime = document.getElementById("sensorProgramEndTime").value;
    var sensortimeMaxToSismiss = document.getElementById("sensortimeMaxToSismiss").value;
    var sensorMaxLogFileTime = document.getElementById("sensorMaxLogFileTime").value;
    var sensorRaspberryCsvPath = document.getElementById("sensorRaspberryCsvPath").value;
    var sensorGeneratedCsvPath = document.getElementById("sensorGeneratedCsvPath").value;
    var sensorLogPath = document.getElementById("sensorLogPath").value;
    var sensorExternalProgramPath = document.getElementById("sensorExternalProgramPath").value;
    var sensorCopyCsvFilesPath = document.getElementById("sensorCopyCsvFilesPath").value;
    var sensorDismissedFilePath = document.getElementById("sensorDismissedFilePath").value;
    var sensorSignalUpperLimit = document.getElementById("sensorSignalUpperLimit").value;
    var sensorTprima = document.getElementById("sensorT-prima").value;
    var sensorVisitorFrame = document.getElementById("sensorVisitorFrame").value;
    var sensorTimeTobeVisitor = document.getElementById("sensorTimeTobeVisitor").value;
    if(validSensor(mac)){
        if(idSensor == null || idSensor == 'default'){
            insertNewSensorInBD(mac, name,model,type,ip, sensorReadSampleTime, sensorProgramStartTime, sensorProgramEndTime,
                sensortimeMaxToSismiss, sensorMaxLogFileTime, sensorRaspberryCsvPath, sensorGeneratedCsvPath,
                sensorLogPath, sensorExternalProgramPath, sensorCopyCsvFilesPath, sensorDismissedFilePath,
                sensorSignalUpperLimit, sensorTprima, sensorVisitorFrame, sensorTimeTobeVisitor, idZone); 
            putMessageInElementSensor("messageSensor", "macSensor", "insertado", "form-control", "alert alert-success form-group");
        
        }else{
            updateSensor(mac, name,model,type,ip, sensorReadSampleTime, sensorProgramStartTime, sensorProgramEndTime,
                sensortimeMaxToSismiss, sensorMaxLogFileTime, sensorRaspberryCsvPath, sensorGeneratedCsvPath,
                sensorLogPath, sensorExternalProgramPath, sensorCopyCsvFilesPath, sensorDismissedFilePath,
                sensorSignalUpperLimit, sensorTprima, sensorVisitorFrame, sensorTimeTobeVisitor); 
            putMessageInElementSensor("messageSensor", "macSensor", "insertado", "form-control", " alert alert-success form-group");   
        }    
        updatePage();
        putClassInDiv("divMacSensor", "form-group"); 
    }
    cleanFormSensor();
}

//validation sensor
function validSensor(mac)
{
    var res = false;
    if(mac!=""){
        if(validMac(mac))
            if(!existMac(mac)){
                if(mac.length <= 17)
                    res = true;
                else
                    putMessageInElementSensor("messageSensor", "divMacSensor", "max size to mac is 17 character", "has-error form-group", "alert alert-danger");
            }else{
                putMessageInElementSensor("messageSensor", "divMacSensor", "Mac already exist", "has-error form-group", "alert alert-danger");    
            }
        else
            putMessageInElementSensor("messageSensor", "divMacSensor", "Mac is invalid", "has-error form-group", "alert alert-danger");    
    }else{
        putMessageInElementSensor("messageSensor", "divMacSensor", "Mac is empty", "has-error form-group", "alert alert-danger");
    }
    return res;
}

//insert new sensor in BD
function insertNewSensorInBD(mac, name,model,type,ip, sensorReadSampleTime, sensorProgramStartTime, sensorProgramEndTime, sensortimeMaxToSismiss, sensorMaxLogFileTime, sensorRaspberryCsvPath, sensorGeneratedCsvPath, sensorLogPath, sensorExternalProgramPath, sensorCopyCsvFilesPath, sensorDismissedFilePath, sensorSignalUpperLimit, sensorTprima, sensorVisitorFrame, sensorTimeTobeVisitor, idZone)
{
    var parametros = {
        "mac": mac,
        "name": name,
        "model": model,
        "type": type,
        "ip": ip,
        "sensorReadSampleTime": sensorReadSampleTime,
        "sensorProgramStartTime": sensorProgramStartTime, 
        "sensorProgramEndTime": sensorProgramEndTime,
        "sensortimeMaxToSismiss": sensortimeMaxToSismiss,
        "sensorMaxLogFileTime": sensorMaxLogFileTime,
        "sensorRaspberryCsvPath": sensorRaspberryCsvPath, 
        "sensorGeneratedCsvPath": sensorGeneratedCsvPath, 
        "sensorLogPath": sensorLogPath, 
        "sensorExternalProgramPath": sensorExternalProgramPath, 
        "sensorCopyCsvFilesPath": sensorCopyCsvFilesPath, 
        "sensorDismissedFilePath": sensorDismissedFilePath, 
        "sensorSignalUpperLimit": sensorSignalUpperLimit, 
        "sensorTprima": sensorTprima, 
        "sensorVisitorFrame": sensorVisitorFrame, 
        "sensorTimeTobeVisitor": sensorTimeTobeVisitor,
        "zone": idZone,
        "function": "insertSensor"
    };
    $.ajax({
        data: parametros,
        url: '../model/sensor.php',
        type: 'post',
        async: false,
        beforeSend: function(){
            //console.log("mac:"+mac);
            
        },
        success: function(response){
            
        },
        error: function(jqXHR, estado, error){
            console.log(estado)
            console.log(error)
        }
    });
}

//update Sensor
function updateSensor(mac, name,model,type,ip, sensorReadSampleTime, sensorProgramStartTime, sensorProgramEndTime, sensortimeMaxToSismiss, sensorMaxLogFileTime, sensorRaspberryCsvPath, sensorGeneratedCsvPath, sensorLogPath, sensorExternalProgramPath, sensorCopyCsvFilesPath, sensorDismissedFilePath, sensorSignalUpperLimit, sensorTprima, sensorVisitorFrame, sensorTimeTobeVisitor)
{
    var parametros = {
        "mac": mac,
        "name": name,
        "model": model,
        "type": type,
        "ip": ip,
        "sensorReadSampleTime": sensorReadSampleTime,
        "sensorProgramStartTime": sensorProgramStartTime, 
        "sensorProgramEndTime": sensorProgramEndTime,
        "sensortimeMaxToSismiss": sensortimeMaxToSismiss,
        "sensorMaxLogFileTime": sensorMaxLogFileTime,
        "sensorRaspberryCsvPath": sensorRaspberryCsvPath, 
        "sensorGeneratedCsvPath": sensorGeneratedCsvPath, 
        "sensorLogPath": sensorLogPath, 
        "sensorExternalProgramPath": sensorExternalProgramPath, 
        "sensorCopyCsvFilesPath": sensorCopyCsvFilesPath, 
        "sensorDismissedFilePath": sensorDismissedFilePath, 
        "sensorSignalUpperLimit": sensorSignalUpperLimit, 
        "sensorTprima": sensorTprima, 
        "sensorVisitorFrame": sensorVisitorFrame, 
        "sensorTimeTobeVisitor": sensorTimeTobeVisitor,
        "idSensor": idSensor,
        "function": "updateSensor"
    };
    $.ajax({
        data: parametros,
        url: '../model/sensor.php',
        type: 'post',
        async: false,
        beforeSend: function(){
        
        },
        success: function(response){
            
        },
        error: function(jqXHR, estado, error){
            console.log(estado)
            console.log(error)
        }
    });
} 

//clean error in sensor
function cleanErrorsSensor()
{
    putClassInDiv("messageSensor", "form-group");
    putClassInDiv("divMacSensor", "form-group");
    document.getElementById("messageSensor").innerHTML="";
}
//get sensor
function getSensor(idSensorVar)
{
    cleanErrorsSensor();
    
    var sensor = getSensorBD(idSensorVar);
    idSensor = idSensorVar;
    document.getElementById("macSensor").value = sensor[0];
    document.getElementById("nameSensor").value = sensor[1];
    document.getElementById("model").value = sensor[2];
    document.getElementById("typeSensor").value = sensor[3];
    document.getElementById("ip").value = sensor[4];
    document.getElementById("sensorReadSampleTime").value = sensor[5];
    document.getElementById("sensorProgramStartTime").value = sensor[6];
    document.getElementById("sensorProgramEndTime").value = sensor[7];
    document.getElementById("sensortimeMaxToSismiss").value = sensor[8];
    document.getElementById("sensorMaxLogFileTime").value = sensor[9];
    document.getElementById("sensorRaspberryCsvPath").value = sensor[10];
    document.getElementById("sensorGeneratedCsvPath").value = sensor[11];
    document.getElementById("sensorLogPath").value = sensor[12];
    document.getElementById("sensorExternalProgramPath").value = sensor[13];
    document.getElementById("sensorCopyCsvFilesPath").value = sensor[14];
    document.getElementById("sensorDismissedFilePath").value = sensor[15];
    document.getElementById("sensorSignalUpperLimit").value = sensor[16];
    document.getElementById("sensorT-prima").value = sensor[17];
    document.getElementById("sensorVisitorFrame").value = sensor[18];
    document.getElementById("sensorTimeTobeVisitor").value = sensor[19];   
}

//get sensor in BD
function getSensorBD(idSensor)
{
    var parametros = {
                    "idSensor": idSensor,
                    "function" : "getSensor"
                };
                
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/sensor.php',
        'data': parametros,
        beforeSend: function(){

        },
        'success': function (request) {
            tmp = eval(request);
           
        }
    });
    return tmp;
    }();
    return return_first;
}

//clean sensor form
function cleanFormSensor()
{
    document.getElementById("nameSensor").value = "";
    document.getElementById("model").value = "";    
    document.getElementById("ip").value = "";
    idSensor = null;
        
}

//delete sensor
function deleteSensor(idSensor)
{
    var textConfirmation = "are you sure? you will delete a sensor";

    if(confirm(textConfirmation))
    {
        var parametros = {
                        "idSensor": idSensor,
                        "function" : "deleteSensor"
                    };
                    
        var return_first = function () {
        var tmp = null;
        $.ajax({
            'async': false,
            'type': "POST",
            'global': false,
            'dataType': 'html',
            'url': '../model/sensor.php',
            'data': parametros,
            beforeSend: function(){
            },
            'success': function (request) {
                tmp = eval(request);
               updatePage();
            }
        });
        return tmp;
        }();
        return return_first;
    }
}

//exist mac
function existMac(mac)
{
    var parametros = {
        "mac": mac,
        "function" : "existMac"
    };
                
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/sensor.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
            tmp = eval(request);
        }
    });
    return tmp;
    }();
    return return_first;
    
}

//update page
function updatePage()
{
    window.location.href = "zonePage.html";
}

