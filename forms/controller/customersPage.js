var idCustomer = null;
var idStore = null;
var sensors = new Array();

$(document).ready(function(){
	completeArrayCustomers();
});

//save cutomer
function saveCustomer()
{
	if(idCustomer == null)
	{
		var email = document.getElementById("email").value;
		var password = document.getElementById("pwd");
		var repeatPassword = document.getElementById("repeatPassword");
		var name = document.getElementById("name").value;
		var address = document.getElementById("address").value;
		var telephone = document.getElementById("telephone").value;
		if(validationsNewCustomer(email, password, repeatPassword, name, address, telephone))
		{
			var customer = insertarNewCustomer(email, password.value, name, address, telephone);
			createDefaultValues(customer);
		}	
	}else{

	}
}

//create default store, zone and sensor whit default values
function createDefaultValues(idCustomer)
{
	var idStore = insertStoreDefault(idCustomer);
	var idZone = insertZoneDefault(idStore);
	var idSensor = insertSensorDefault(idZone);
}

//load account page
function loadAccountPage()
{
	$("#subContainer").load("accountPage.html");
}

//insert new customer
function insertarNewCustomer(email, password, name, address, telephone)
{	
	var parametros = {
					"email": email,
					"password": password,
					"name": name,
					"address": address,
					"telephone": telephone,
					"function": "insertCustomer"
				};
	
	var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/acount.php',
        'data': parametros,
        beforeSend: function(){
		},
        'success': function (request) {
         	tmp = eval(request);
         	updatePage();
        },
		error: function(jqXHR, estado, error){
			console.log(estado)
			console.log(error)
		}
    });
    return tmp;
	}();

	return return_first;
}

//complete array of customer
function completeArrayCustomers()
{
	$("#tbodyCustomers").empty();
	var customers = getCustomersArray();
	
	for(var i = 0; i < customers.length; i++)
	{
		insertItemInTableCustomers(customers[i]);
	}
}

//get customers array
function getCustomersArray()
{
	var parametros = {
					"function": "getCustomers"
				};
				
	var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/customers.php',
        'data': parametros,
        beforeSend: function(){

		},
        'success': function (request) {
         	tmp = eval(request);
        	
        }
    });
    return tmp;
	}();
	return return_first;
}

//insert item to table customers
function insertItemInTableCustomers(customer)
{
	var table = document.getElementById("idTableCustomers");
    var row = table.insertRow(1);
   
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2)
    var cell4 = row.insertCell(3)
    var cell5 = row.insertCell(4)
    cell1.innerHTML = '<a href= "../pages/customerPage.html" onclick="saveInSesion('+customer[0]+')">'+customer[2]+'</a>';
    cell2.innerHTML = '<a href= "../pages/customerPage.html" onclick="saveInSesion('+customer[0]+')">'+customer[4]+'</a>';
    cell3.innerHTML = customer[1];
    cell4.innerHTML = '<button type="submit" class="btn btn-primary" onclick="goToDashboard('+customer[0]+')">Dashboard</button>';     
    cell5.innerHTML = '<button type="submit" class="btn btn-danger" onclick="deleteCustomer('+customer[0]+')">Delete</button>';     
}

//go to dashboard
function goToDashboard(idCustomer)
{
	var parametros = {
						"idCustomer": idCustomer,
						"function" : "setSessionCustomer"
					};
					
		var return_first = function () {
	    var tmp = null;
	    $.ajax({
	        'async': false,
	        'type': "POST",
	        'global': false,
	        'dataType': 'html',
	        'url': '../model/login.php',
	        'data': parametros,
	        beforeSend: function(){
			},
	        'success': function (request) {
	        	tmp = eval(request);
				window.open(
					'../pages/dashboardCustomer.php',
					'_blank' // <- This is what makes it open in a new window.
				);
			}
	    });
	    return tmp;
		}();
		return return_first;
}

//save in session
function saveInSesion(idCustomer)
{
	writeCookie('idCustomer', idCustomer, 1);
}

//delete customer
function deleteCustomer(idCustomer)
{
	var textConfirmation = "are you sure? you will delete all the customer's elements ";

	if(confirm(textConfirmation))
	{
		 
		var parametros = {
						"idCustomer": idCustomer,
						"function" : "deleteCustomer"
					};
					
		var return_first = function () {
	    var tmp = null;
	    $.ajax({
	        'async': true,
	        'type': "POST",
	        'global': false,
	        'dataType': 'html',
	        'url': '../model/customers.php',
	        'data': parametros,
	        beforeSend: function(){
			},
	        'success': function (request) {
	         	tmp = eval(request);
	         	updatePage();
	        },
	        error: function(jqXHR, estado, error){
				console.log(estado)
				console.log(error)
			}
	    });
	    return tmp;
		}();
		return return_first;
		
	}
	
}

//update page
function updatePage()
{
	window.location.href = "customersPage.html";
}
