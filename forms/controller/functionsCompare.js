$(document).ready(function(){
	fullLocations();
    showCompare(); 
});
var opts = {
     position: 'relative' // Element positioning
    ,color:'#1781ad'
    };
//show compare
function showCompare()
{
    var dateInit = new Date($("#fecha").datepicker("getDate"));
    var dateFinal = new Date($("#fecha2").datepicker("getDate"));
    dateFinal.setDate(dateFinal.getDate()+1);// this is for the query in the bd
    dateInit = changeFormatyyyymmdd(dateInit);
    dateFinal = changeFormatyyyymmdd(dateFinal);
	
    var kpi = document.getElementById('idKpi').value;
    var stores = document.getElementsByName("nameKpi");
    var storesChecked = new Array();
    
    for(var i = 0; i < stores.length; i++)
        if(stores[i].checked)
            storesChecked.push(stores[i].value);
    var res = '';
    
    switch(kpi) {
    case 'Customers':
        res = getResultsCompare(storesChecked, dateInit, dateFinal, 'compareCustomers');    
        drawCompare(res, ['Customers']);
        break;
    case 'Outside Opportunity':
        res = getResultsCompare(storesChecked, dateInit, dateFinal, 'compareOutsideOpportunity');    
        drawCompare(res, ['avarage']);
    break;
    case 'Outside Conversion':
        res = getResultsCompare(storesChecked, dateInit, dateFinal, 'compareOutsideConversion');    
        drawCompare(res, ['avarage']);
        break;
    case 'Avarage Duration':
        res = getResultsCompare(storesChecked, dateInit, dateFinal, 'compareAvarageDuration');    
        drawCompare(res, ['minuts']);
        break;  
    case 'Bounce Rate':
        res = getResultsCompare(storesChecked, dateInit, dateFinal, 'compareBounceRate');    
        drawCompare(res, ['avarage']);
        break;
    case 'Repeat Customers':
        res = getResultsCompare(storesChecked, dateInit, dateFinal, 'compareRepeatCustomers');    
        drawCompare(res, ['avarage']);
        break;
    case 'News Customers':
        res = getResultsCompare(storesChecked, dateInit, dateFinal, 'compareNewCustomers');    
        drawCompare(res, ['avarage']);
        break;                
    default:
        break;
    }
    
}

//draw graphic compare
function drawCompare(res, nameData)
{
    var datas = [];
    
    for(var i = 0; i < res.length; i++){
        datas.push({
          y: res[i][1],
          a: res[i][0]
        });
    }
    Morris.Bar({
        element: 'morris-bar-chart',
        data: datas,
        xkey: 'y',
        ykeys: ['a', ],
        labels: nameData,
        hideHover: 'auto',
        resize: true
    });
}

//get results comapare
function getResultsCompare(listStores, dateInit, dateFinal, functionName)
{
     var parametros = {
        "storesChecked" : listStores,
        "dateInit" : dateInit,
        "dateFinal" : dateFinal,
        "function" : functionName
    };            
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/compare.php',
        'data': parametros,
        beforeSend: function(){
            $(('#morris-bar-chart')).html("");
            var target = document.getElementById('morris-bar-chart');
            var spinner = new Spinner(opts).spin(target);
        },
        'success': function (request) {
            $(('#morris-bar-chart')).html("");
            tmp = eval(request);
            
        },
        error: function(jqXHR, status, error){
            console.log(status)
            console.log(error)
        }
    });
    return tmp;
    }();
    return return_first;     
}

//fill location
function fullLocations()
{
    var names = getStoresNames(); 
    var container = document.getElementById('idLocations');
    for (var i = 0; i < names.length ; i++) {
        var div = document.createElement( 'div' );
        div.className = "checkbox";
        div.innerHTML = "<label><input checked='true' name='nameKpi' type='checkbox' value='"+names[i][0]+"'>"+names[i][1]+"</label>"; 
        container.appendChild(div);
    }
    
}

//get stores names
function getStoresNames()
{
    var parametros = {
         "function" : "getStoresNames"
    };            
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/store.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
            tmp = eval(request);
        },
        error: function(jqXHR, status, error){
            console.log(status)
            console.log(error)
        }
    });
    return tmp;
    }();
    return return_first;  
}

//change format date yyyy-mm-dd
function changeFormatyyyymmdd(date)
{   
    res = (date.getFullYear() + '-' + (date.getMonth()+1) + '-' +  date.getDate());
    return res;
}

//change All Location
function changeAllLocation()
{
    var allLocation = document.getElementById('idAllLocations');
    if(allLocation.checked){
        chekAllStores(true);
        showCompare();
    }
    else
        chekAllStores(false);
    
}

//check all stores
function chekAllStores(checked)
{
    var stores = document.getElementsByName("nameKpi");
    for(var i = 0; i < stores.length; i++)
        stores[i].checked = checked;
            
}