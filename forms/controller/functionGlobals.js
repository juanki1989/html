
//============================= LOGIN ===========================
// valid login
function login(type)
{   
    var email = document.getElementById("emailLogin").value;
    var password = document.getElementById("passwordLogin").value;
    if(validEmail(email)){
        putClassInDiv("divEmail", "form-group");
        if(email == "" || password == "")
            putMessageInElement("message", "","The email or password is empty", "");
        else{
            if(type == 'customer')
                loginCustomer(email,password);    
            else
                loginAdmin(email,password);
        }
    }else
            putMessageInElement("message", "divEmail" ,"The email is invalid, example: email@exaple.com", "has-error form-group");            
}

//login Admin User
function loginAdmin(email, password)
{
    if(existUser(email, password, 'existUser')){
        writeCookie('admin','admin', 1);
        if(document.getElementById("remember").value){
            writeCookie('emailLoginAdmin',email, 14);
            writeCookie('passwordLoginAdmin',password, 14);
        }
        window.location.href = "customersPage.html";
    }else{
        var email = document.getElementById("emailLogin").value = "";
        var password = document.getElementById("passwordLogin").value = "";
        putMessageInElement("message", "", "The email or password is not correct", "");
    }
}

//login Customer User
function loginCustomer(email, password)
{
    var idCustomer = existUser(email, password, 'existUserCustomer');
    if(idCustomer != "0"){
        if(document.getElementById("remember").value){
            writeCookie('emailLoginCustomer',email, 14);
            writeCookie('passwordLoginCustomer',password, 14);
        }
        goToDashboard(idCustomer);
        writeCookie('idCustomer',idCustomer, 1);
    }else{
        var email = document.getElementById("emailLogin").value = "";
        var password = document.getElementById("passwordLogin").value = "";
        putMessageInElement("message", "", "The email or password is not correct", "");    
    }
}

//close session
function logout()
{
    writeCookie('admin','', 1000);
    writeCookie('idCustomer','', 1000);
}

// question, exist user?
function existUser(email, password, functionName)
{
        var parametros = {
                        "email": email,
                        "password": password,
                        "function" : functionName
                    };
                    
        var return_first = function () {
        var tmp = null;
        $.ajax({
            'async': false,
            'type': "POST",
            'global': false,
            'dataType': 'html',
            'url': '../model/login.php',
            'data': parametros,
            beforeSend: function(){
            },
            'success': function (request) {
                tmp = eval(request);
            }
        });
        return tmp;
        }();
        return return_first;
}

//load page
function load(page)
{
    $("#page-wrapper").load(page);
}

//set Id customer in session
function setIdCustomer()
{
    var idCustomerVar = getIdSessionCustomer();
    idCustomer = idCustomerVar;
    writeCookie('idCustomer',idCustomer, 1);
}

//get Id Customer from session
function getIdSessionCustomer()
{
    var parametros = {
                    "function": "getIdSessionCustomer"
                };
                
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/customers.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
            tmp = eval(request);
        }
    });
    return tmp;
    }();
    return return_first;
}

//Show layout only for Admnin User
function ShowAdminLayout()
{
    $("#customersMenu").show();
    $("#customersDashboardMenu").show();
}

//go to dashboard User
function goToDashboard(idCustomer)
{
        var parametros = {
                        "idCustomer": idCustomer,
                        "function" : "setSessionCustomer"
                    };
                    
        var return_first = function () {
        var tmp = null;
        $.ajax({
            'async': false,
            'type': "POST",
            'global': false,
            'dataType': 'html',
            'url': '../model/login.php',
            'data': parametros,
            beforeSend: function(){
            },
            'success': function (request) {
                tmp = eval(request);
                    window.location.href = "../pages/dashboardCustomer.php";
            }
        });
        return tmp;
        }();
        return return_first;
}

// ============================= VALIDACIONES =======================================
//put some Class In some Div
function putClassInDiv(div, cssClass)
{
    document.getElementById(div).className = cssClass;
}

//put Message In Element
function putMessageInElement(id, divId, message, cssClass)
{
    var divMessague = document.getElementById(id);
    var divDiv = document.getElementById('div');
    if(divDiv != null)
        divMessague.removeChild(divDiv);
    
    var div = document.createElement( 'div' );
    div.className = "alert alert-danger";
    div.id = "div";
    div.appendChild(document.createTextNode(message));
    
    divMessague.appendChild(div);

    if(divDiv != "" && cssClass != "")
    {
        var divField = document.getElementById(divId);
        divField.className = cssClass;           
    }
}

//put Message In Element Store
function putMessageInElementStore(id, divId, message, cssClass)
{
    var divMessague = document.getElementById(id);
    var divDiv = document.getElementById('divStore');
    if(divDiv != null)
        divMessague.removeChild(divDiv);
    
    var div = document.createElement( 'div' );
    div.className = "alert alert-danger";
    div.id = "divStore";
    div.appendChild(document.createTextNode(message));
    
    divMessague.appendChild(div);

    if(divDiv != "" && cssClass != "")
    {
        var divField = document.getElementById(divId);
        divField.className = cssClass;           
    }
}
//put Message In Element Sensor
function putMessageInElementSensor(id, divId, message, cssClass, cssClassDiv)
{
    var divMessague = document.getElementById(id);
    var divDiv = document.getElementById('divSensor');
    if(divDiv != null)
        divMessague.removeChild(divDiv);
    
    var div = document.createElement( 'div' );
    div.className = cssClassDiv;
    div.id = "divSensor";
    div.appendChild(document.createTextNode(message));
    
    divMessague.appendChild(div);

    if(divDiv != "" && cssClass != "")
    {
        var divField = document.getElementById(divId);
        divField.className = cssClass;           
    }
}

//put Message In Element Succes
function putMessageInElementSucces(idMessage, idDivInMessage, cssClass, message)
{
    var divMessague = document.getElementById(idMessage);
    var divDiv = document.getElementById(idDivInMessage);
    if(divDiv != null)
        divMessague.removeChild(divDiv);
    
    var div = document.createElement(idDivInMessage);
    div.className = cssClass;
    div.id = idDivInMessage;
    div.appendChild(document.createTextNode(message));
    
    divMessague.appendChild(div);
}

//validation for email
function validEmail(email)
{
    var res = false;
    if(email != "")
    {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        res = re.test(email);
        if(res)
            res = true;
    }  
    return res;
}

//validation for password and repeat password
function validPass(pass, repeatPass)
{
    var res = false;
    if(pass.value != ""){
        putClassInDiv("divPassword", "form-group");
        if(repeatPass.value != ""){
            putClassInDiv("divPasswordRepeat", "form-group");
            if(pass.value == repeatPass.value){
                res = true;
                putClassInDiv("divPassword", "form-group");
                putClassInDiv("divPasswordRepeat", "form-group");
            }
            else{
                putMessageInElement("message", "divPassword", "the passwords are differents", "has-error form-group");
                putMessageInElement("message", "divPasswordRepeat", "the passwords are differents", "has-error form-group");
            }
        }else
            putMessageInElement("message", "divPasswordRepeat", "The repeat password is empty", "has-error form-group");
    }else
        putMessageInElement("message", "divPassword", "The password is empty", "has-error form-group");
                
    return res;
}

//question, exist email customer?
function existMail(email)
{
    var parametros = {
                    "email": email,
                    "function" : "existMail"
                };
                
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/signUp.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
            tmp = eval(request);
        }
    });
    return tmp;
    }();
    return return_first;
}


//question, exist email Admin
function existEmailAdmin(email)
{
    var parametros = {
                    "email": email,
                    "function" : "existEmailAdmin"
                };
                
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/login.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
            tmp = eval(request);
        }
    });
    return tmp;
    }();
    return return_first;
}


//valid mac?
function validMac(mac)
{
    var regex = /^([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])$/;
    return regex.test(mac);
}

//validations New Customer
function validationsNewCustomer(email, password, repeatPassword, name, address, telephone)
{
    var res = false;
    if(name != "")
        if(validEmail(email)){
            putClassInDiv("divEmail", "form-group");
            if(validPass(password, repeatPassword)){
                if((!existMail(email))&&(!existEmailAdmin(email))){
                    putClassInDiv("divEmail", "form-group");
                    res = true;
                }else
                    putMessageInElement("message", "divEmail", "The Email already exists", "has-error form-group");
            }
        }else
            putMessageInElement("message", "divEmail" ,"The email is invalid, example: email@exaple.com", "has-error form-group");
    else
        putMessageInElement("message", "divName" ,"The name is empty", "has-error form-group");
    return res; 
}

//function to change Password Customer
function changePasswordCustomer()
{
    var oldPassword = document.getElementById("oldPassword");
    var newPassword = document.getElementById("newPassword");
    var newRepeatPassword = document.getElementById("repeatNewPassword");
    idCustomer = readCookie("idCustomer");
    if(validPass(newPassword, newRepeatPassword))
        if(existPassword(idCustomer, oldPassword.value)){
            if(changePassword(idCustomer, newPassword.value)){
                putMessageInElementSucces("message", "div", "alert alert-success form-group", "Updated");
                setInterval(function () {window.location.href = "../pages/dashboardCustomer.php";}, 1000);
                
            }
        }else
            putMessageInElement("message", "divOldPassword" ,"Wrong old password", "has-error form-group");        
}

//change Password
function changePassword(idCustomer, newPassword)
{
    var parametros = {
                    "idCustomer": idCustomer,
                    "newPassword": newPassword,
                    "function" : "changePassword"
                };
                
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/customers.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
            tmp = eval(request);
        }
    });
    return tmp;
    }();
    return return_first;   
}

//========================== FUNCIONES GENERALES ==============
//write value in session
function writeCookie(name,value,days) {
    var date, expires;
    if (days) {
        date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires=" + date.toGMTString();
            }else{
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

//read session from customer
function readCookie(name) {
    var i, c, ca, nameEQ = name + "=";
    ca = document.cookie.split(';');
    for(i=0;i < ca.length;i++) {
        c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length,c.length);
        }
    }
    return '';
}

//reset Password Customer
function resetPasswordCustomer(code, email)
{
    var newPassword = document.getElementById("password");
    var newRepeatPassword = document.getElementById("passwordRepeat");
    if(validPass(newPassword, newRepeatPassword))
        if(validCode(code, email)){
            //putClassInDiv("div", "form-group");
            if(resetPassword(email, newPassword.value)){
                putMessageInElementSucces("message", "div", "alert alert-success form-group", "Updated");
                setInterval(function () {window.location.href = "../pages/dashboardCustomer.php";}, 1000);        
            }else{
                console.log("a ocurrido un error");
            }
        }else
            putMessageInElement("message", "div" ,"to reset code has expired", "alert alert-danger");
}

//sent mail to reset password
function sendMyPassword()
{
    var code = Math.round(Math.random()*10000000);
    var email = document.getElementById("email").value;
    if(validEmail(email)){
        putClassInDiv("divEmail", "form-group");
        if(existMail(email)){
            putClassInDiv("divEmail", "form-group");
            if(insertResetCode(code, email)){
                
                putMessageInElementSucces("message", "div", "alert alert-success form-group", "Sent Email");
                setInterval(function () {window.location.href = "../../index.html";}, 1000);               
            }else
                alert("no envio");

        }else
            putMessageInElement("message", "divEmail" ,"Email does not exist", "has-error form-group");    
    }else
        putMessageInElement("message", "divEmail" ,"Email is invalid, example: email@exaple.com", "has-error form-group");
}

//========================== VALIDACIONES ==============
//validations Edit Customer
function validationsEditCustomer(email, password, repeatPassword, name, address, telephone, emailCustomer)
{
    var res = false;
    if(name != ""){
        putClassInDiv("divEmail", "form-group");
        if(validEmail(email)){
            putClassInDiv("divEmail", "form-group");
            if(hidePasswords() || validPass(password, repeatPassword)){
                if(emailCustomer == email || ((!existMail(email))||(!existEmailAdmin(email)))){
                    putClassInDiv("divEmail", "form-group");
                    res = true;
                    putMessageInElementSucces("message", "div", "alert alert-success form-group", "Updated");
                }else
                    putMessageInElement("message", "divEmail", "The Email already exists", "has-error form-group");
            }
        }else
            putMessageInElement("message", "divEmail" ,"The email is invalid, example: email@exaple.com", "has-error form-group");
    }else
        putMessageInElement("message", "divName" ,"The name is empty", "has-error form-group");        
    return res; 
}

//hide field password
function hidePasswords()
{
    return !$("#divPassword").is(":visible"); 
}

//========================== CONSUTAS BASE DE DATOS ==============
//password correct for customer user
function existPassword(idCustomer, oldPassword)
{
    var parametros = {
                    "idCustomer": idCustomer,
                    "oldPassword": oldPassword,
                    "function": "existPassword"
                };
    
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/customers.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
            tmp = eval(request);
        }
    });
    return tmp;
    }();
    return return_first;
}

//valid code reset password
function validCode(code, email)
{
    var parametros = {
                    "code": code,
                    "email": email,
                    "function": "validCode"
                };
    
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/customers.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
            tmp = eval(request);
           
        }
    });
    return tmp;
    }();
    return return_first;
}

//reset Password 
function resetPassword(email, newPassword)
{
    var parametros = {
                    "email": email,
                    "newPassword": newPassword,
                    "function" : "changePasswordWhitEmail"
                };
                
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/customers.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
            tmp = eval(request);
        }
    });
    return tmp;
    }();
    return return_first;   
}

//insert Reset Code
function insertResetCode(code, email)
{
    var parametros = {
                    "code": code,
                    "email": email,
                    "function" : "insertResetCode"
                };
                
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/customers.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
            tmp = eval(request);
        }
    });
    return tmp;
    }();
    return return_first;      
}

//====================FUNCIONES STORE=======================
//insert Store Default
function insertStoreDefault(idCustomer)
{
    var parametros = {
                    "idCustomer": idCustomer,
                    "function": "insertStoreDefault"
                };
                
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/store.php',
        'data': parametros,
        beforeSend: function(){
        
        },
        'success': function (request) {

            tmp = eval(request);
            
        }
    });
    return tmp;
    }();
    return return_first;
}

//====================FUNCIONES ZONE=======================
//insert Zone Default
function insertZoneDefault(idStore)
{ 
   var parametros = {
                    "idStore" : idStore,
                    "function": "insertZoneDefault"
                };
                
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/zone.php',
        'data': parametros,
        beforeSend: function(){
        
        },
        'success': function (request) {

            tmp = eval(request);
            
        }
    });
    return tmp;
    }();
    return return_first;
}

//====================FUNCIONES SENSOR=======================
//insert Sensor Default
function insertSensorDefault(idZone)
{
    var mac = Math.round(Math.random()*100000);
    var parametros = {
                    "idZone" : idZone,
                    "mac": mac,
                    "function": "insertSensorDefault"
                };
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/sensor.php',
        'data': parametros,
        beforeSend: function(){
        
        },
        'success': function (request) {

            tmp = eval(request);
            
        }
    });
    return tmp;
    }();
    return return_first;
}