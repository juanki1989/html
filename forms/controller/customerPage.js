var idCustomer = null;
var idStore = null;
var sensors = new Array();
var emailCustomer = "";

$(document).ready(function(){
	if(readCookie('admin') == "admin")
		ShowAdminLayout();
	idCustomer = readCookie('idCustomer');
	completeCustomer(idCustomer);
	completeArrayStores(idCustomer);
	
});

//full attributes of a customer
function completeCustomer(idCustomer)
{
	var customer = getCustomer(idCustomer);
	document.getElementById("name").value = customer[2];
	document.getElementById("email").value = customer[4];
	emailCustomer = customer[4];
	document.getElementById("address").value = customer[1];	
	document.getElementById("telephone").value = customer[3];
}

//get custoemr
function getCustomer(idCustomer)
{
	var parametros = {
					"idCustomer": idCustomer,
					"function" : "getCustomer"
				};
				
	var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/customers.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
		 	tmp = eval(request);
        }
    });
    return tmp;
	}();
	return return_first;
}

//save customer
function saveCustomer()
{
	var email = document.getElementById("email").value;
	var password = document.getElementById("password");
	var passwordRepeat = document.getElementById("passwordRepeat");
	var name = document.getElementById("name").value;
	var address = document.getElementById("address").value;
	var telephone = document.getElementById("telephone").value;
	
	if(idCustomer == null)
	{
		insertarNewCustomer(name, email, password.value, address, telephone);
	}else{
		if(validationsEditCustomer(email, password, passwordRepeat, name, address, telephone, emailCustomer))
			updateCustomer(name, email, password.value, address, telephone, idCustomer);
	}
}

//show fields of passwords
function showFieldsPassword()
{
	$("#divPasswordRepeat").show();
	$("#divPassword").show();
	$("#changePassword").hide();
	$("#hidePasswordFelds").show();
}

//hide fields of password
function hidePasswordFelds()
{
	$("#divPasswordRepeat").hide();
	$("#divPassword").hide();
	$("#changePassword").show();
	$("#hidePasswordFelds").hide();
	document.getElementById("password").value = "";
	document.getElementById("passwordRepeat").value = "";	
}

//udpate customer
function updateCustomer(name, email, password, address, telephone, idCustomer)
{
	var parametros = {
					"email": email,
					"name": name,
					"password": password,
					"address": address,
					"telephone": telephone,
					"idCustomer": idCustomer,
					"function": "updateCustomer"
				};
	$.ajax({
					data: parametros,
					url: '../model/customers.php',
					type: 'post',
					beforeSend: function(){
						
					},
					success: function(response){
					},
					error: function(jqXHR, estado, error){
						console.log(estado)
						console.log(error)
					}
				});                 

}

//save store
function saveStore()
{	if(idStore == null)
	{
		var email = document.getElementById("emailStore").value;
		var name = document.getElementById("nameStore").value;
		var address = document.getElementById("addressStore").value;
		var telephone = document.getElementById("telephoneStore").value;
		var responsible = document.getElementById("responsibleStore").value;
		var geo = document.getElementById("geoStore").value;
	
		if(name != ""){
			idStore = insertNewStore(name, email, address, telephone, responsible, geo, idCustomer);
			updatePage();
		}else
			putMessageInElementStore("messageStore", "nameStore", "The name is empty", "has-error form-control");
		
	}else{

	}
	
}

//insert new store in BD
function insertNewStore(name, email, address, telephone, responsible, geo, idCustomer)
{
	var parametros = {
					"email": email,
					"responsible": responsible,
					"name": name,
					"address": address,
					"geo": geo,
					"telephone": telephone,
					"idCustomer": idCustomer
				};
				
	var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/store.php',
        'data': parametros,
        beforeSend: function(){
		
		},
        'success': function (request) {

         	tmp = eval(request);
        	
        }
    });
    return tmp;
	}();
	return return_first;
}

//complete Arrat of Stores
function completeArrayStores(idCustomer)
{

	var stores = getStoresArray(idCustomer);
	for(var i = 0; i < stores.length; i++)
	{
		insertItemInTableStores(stores[i]);
	}
}

//get Array of Stores
function getStoresArray(idCustomer)
{
	var parametros = {
					"idCustomer": idCustomer
				};
				
	var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/stores.php',
        'data': parametros,
        beforeSend: function(){
		},
        'success': function (request) {
         	tmp = eval(request);
        }
    });
    return tmp;
	}();
	return return_first;
}

//insert item in table Stores
function insertItemInTableStores(store)
{
	var table = document.getElementById("idTableStores");
    var row = table.insertRow(1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2)
    var cell4 = row.insertCell(3)
    cell1.innerHTML = '<a href= "../pages/storePage.html" onclick="saveInSesion('+store[0]+')">'+store[1]+'</a>';
    cell2.innerHTML = store[4];
    cell3.innerHTML = store[2];  
    cell4.innerHTML = '<button type="submit" class="btn btn-danger" onclick="deleteStore('+store[0]+')">Delete</button>';     
}

//save in Session id store
function saveInSesion(idStore)
{
	writeCookie('idStore', idStore, 1);
}

//dele store in BD
function deleteStore(idStore)
{
	var textConfirmation = "are you sure? you will delete all the Store's elements ";

	if(confirm(textConfirmation))
	{
		var parametros = {
						"idStore": idStore,
						"function" : "deleteStore"
					};
					
		var return_first = function () {
	    var tmp = null;
	    $.ajax({
	        'async': false,
	        'type': "POST",
	        'global': false,
	        'dataType': 'html',
	        'url': '../model/store.php',
	        'data': parametros,
	        beforeSend: function(){
			},
	        'success': function (request) {
	         	//alert(request);
	         	tmp = eval(request);
	         	updatePage();
	        }
	    });
	    return tmp;
		}();
		return return_first;
	}
}

//update page
function updatePage()
{
	window.location.href = "customerPage.html";
}