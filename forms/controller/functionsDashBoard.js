$(document).ready(function(){
	//setFiltrosInSession(currentDate(), currentDate(), $("#periods").val(), $("#locations").val());
	limitsDates();
	startProcess(true);
	
});

//====== variables globales
var datesCurrentPeriod = [];
var datesPreviousPeriod = [];

//==== bloques ajax
	var loadOutsideOppotunity;
	var loadOutsideConversion;
	var loadAverageDuration;
	var loadNewCustomers;
	var loadRepetCustomers;
	var loadBounceRate;
				
	var DaysAvarageDuration=new Array('MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN');	
	var loadAverageDurationGraphic;
	var loadOutsideConversiongraphic;
	var loadOutsideOppotunityGraphic;
//-=========
var opts = {
	 position: 'relative' // Element positioning
 	,color:'#1781ad'
	};

function currentDate()
{
	var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 
    var today = dd+'/'+mm+'/'+yyyy;
    
    return today;
}

//limits date	
function limitsDates()
{
	var minDateVar2 = '<?php include("../model/minDate.php"); ?>';
        var daysToAdd = -1;
    	$("#fecha").datepicker({
    	dateFormat: "dd/mm/yy",
        maxDate: -1,
        minDate: minDateVar2,
        onSelect: function (selected) {
        	var dtMax = new Date(selected);
            dtMax.setDate(dtMax.getDate()); 
            var dd = dtMax.getDate();
            var mm = dtMax.getMonth();
            var y = dtMax.getFullYear();
            var dtFormatted = dd + '/'+ mm + '/'+ y;
            $("#fecha2").datepicker("option", "dateFormat", "dd/mm/yy");
            $("#fecha2").datepicker("option", "minDate", selected);
        }

    });

    $("#fecha2").datepicker({
   		dateFormat: "dd/mm/yy",
        maxDate: -1,
        minDate: $("#fecha").datepicker({dateFormat: 'dd/mm/yy'}).val()
    });
}

//inlcude library
function include(fileName){
  document.write("<script type='text/javascript' src='"+fileName+"'></script>" );
}

//set filtros session
function setFiltrosInSession(dateStart, dateEnd, periodos, location)
{
	var parameters = {
		"fecha":dateStart,
		"fecha2":dateEnd,
		"periods":periodos,
		"sel_locations":location
	};
	$.ajax({
		data: parameters,
		url: '../model/actFiltros.php',
		type: 'post',
		sync: true,
		beforeSend: function(){
		},
		success: function(response){
		},
		error: function(jqXHR, status, error){
			console.log(status)
			console.log(error)
		}
	});
}
//start process to show dashboard
function startProcess(firstTime)
{
	updateRangeDatesLocal();
	setFiltrosInSession($("#fecha").datepicker({dateFormat: 'dd/mm/yy'}).val(),
						$("#fecha2").datepicker({dateFormat: 'dd/mm/yy'}).val(),
						$("#periods").val(), 
						$("#locations").val());
				
	
	calculateDates();
	if(firstTime)
		loadDatasFirstTime();
	else
		loadDatas();
}
// update range dates local
function updateRangeDatesLocal()
{
	$.ajax({
		url: '../model/minDate.php',
		type: 'post',
		async: false,
		beforeSend: function(){
			
		},
		success: function(response){
			$("#fecha").datepicker("option", "minDate", response);
		},
		error: function(jqXHR, status, error){
			console.log(status)
			console.log(error)
		}
	});
}

//calculate dates
function calculateDates()
{
	s = new Date($("#fecha").datepicker("getDate"));
	e = new Date($("#fecha2").datepicker("getDate"));
	datesCurrentPeriod = generateDates(s, e);
	
	var auxDateStartPrevious = s;
	auxDateStartPrevious.setMonth(auxDateStartPrevious.getMonth()-1);
	datesPreviousPeriod = generateDates(auxDateStartPrevious, s);
}

//generate range for dates for graphics
function generateDates(s, e)
{
	var a = [];
	while(s <= e) {
        a.push(changeFormat(s));
        s = new Date(s.setDate(s.getDate() + 1)) 
    }
    return a;	
}

//change format date dd/mm/yyyy
function changeFormat(date)
{	
	res = (date.getDate() + '/' + (date.getMonth()+1) + '/' +  date.getFullYear());
	return res;
}

//question and abort process dashboard
function questionAndAbort(variable)
{
	if(typeof(variable) !== "undefined" && variable !== null){
		variable.abort();	
	}	
}

//load datas to dashboard
function loadDatas()
{
	
	questionAndAbort(loadOutsideOppotunity);
	questionAndAbort(loadOutsideConversion);
	questionAndAbort(loadAverageDuration);
	questionAndAbort(loadNewCustomers);
	questionAndAbort(loadRepetCustomers);
	questionAndAbort(loadBounceRate);

	loadOutsideOppotunity = loadBloque('../model/outsideOpportunityCompare.php', "outsideOpportunity");
	loadOutsideConversion = loadBloque('../model/outsideConversionCompare.php', "outsideConversion");
	loadAverageDuration = loadBloque('../model/averageCompare.php', "averageDuration");
	loadRepetCustomers = loadBloque('../model/repeatCustomersCompare.php', "repetCustomers");
	loadBounceRate = loadBloque('../model/bounCefCompare.php', "bounceRate");
			
	loadAverageDurationGraphic = ajaxAvarageDuration('../model/graphics/avarageDuration/avarageDuration.php', "avg_div", DaysAvarageDuration);
	loadOutsideConversiongraphic = ajaxGraphicLines('../model/graphics/outsideConversion/outsideConversion.php', "conv_div");
	loadOutsideOppotunityGraphic = ajaxGraphicLines('../model/graphics/outsideOportunity/outsideOpportunity.php', "opportunity_div");		
	
}

//first time to load datas for dashboard
function loadDatasFirstTime()
{
	
	questionAndAbort(loadOutsideConversiongraphic);
	questionAndAbort(loadOutsideOppotunityGraphic);
	questionAndAbort(loadRepetCustomers);
	
	
	var dateEnd = new Date();
	dateEnd.setDate(dateEnd.getDate()-1);//always see one day ago
	$("#fecha2").datepicker("setDate", dateEnd);
	$("#fecha").datepicker("setDate", dateEnd);
	limitsDates();
	setFiltrosInSession($("#fecha").datepicker({dateFormat: 'dd/mm/yy'}).val(),
    					$("#fecha2").datepicker({dateFormat: 'dd/mm/yy'}).val(),
						$("#periods").val(), 
						$("#locations").val());
	loadOutsideOppotunity = loadBloque('../model/outsideOpportunityCompare.php', "outsideOpportunity");
	loadOutsideConversion = loadBloque('../model/outsideConversionCompare.php', "outsideConversion");
	loadAverageDuration = loadBloque('../model/averageCompare.php', "averageDuration");
	loadRepetCustomers = loadBloque('../model/repeatCustomersCompare.php', "repetCustomers");
	loadBounceRate = loadBloque('../model/bounCefCompare.php', "bounceRate");
	

	dateEnd = new Date($("#fecha2").datepicker("getDate"));
	dateEnd.setMonth(dateEnd.getMonth()-1);
    var dd = dateEnd.getDate();
    var mm = dateEnd.getMonth()+1;
    var y = dateEnd.getFullYear();
    var dtFormatted = dd + '/'+ mm + '/'+ y;
    console.log("inidate:"+dtFormatted);
    setFiltrosInSession(dtFormatted,
    					$("#fecha2").datepicker({dateFormat: 'dd/mm/yy'}).val(),
						$("#periods").val(), 
						$("#locations").val());

	s = dateEnd;
	e = new Date($("#fecha2").datepicker("getDate"));
	datesCurrentPeriod = generateDates(s, e);
	

	var auxDateStartPrevious = s;
	auxDateStartPrevious.setMonth(auxDateStartPrevious.getMonth()-1);
	datesPreviousPeriod = generateDates(auxDateStartPrevious, s);

	loadAverageDurationGraphic = ajaxAvarageDuration('../model/graphics/avarageDuration/avarageDuration.php', "avg_div", DaysAvarageDuration);
	loadOutsideConversiongraphic = ajaxGraphicLines('../model/graphics/outsideConversion/outsideConversion.php', "conv_div");
	loadOutsideOppotunityGraphic = ajaxGraphicLines('../model/graphics/outsideOportunity/outsideOpportunity.php', "opportunity_div");		
}

// draw avarage duration
function ajaxAvarageDuration(url, container, dias)
{
	
	var request = $.ajax({
	url: url,
	type: 'post',
	async: true,
	beforeSend: function(){
		$(('#'+container)).html("");
		var target = document.getElementById(container);
		var spinner = new Spinner(opts).spin(target);
	},
	success: function(response){
		var variable = eval(response);
		dibujarAvarageDuration(variable[0], variable[1], dias, container,'corechart');
	},
	error: function(jqXHR, status, error){
		console.log(status)
		console.log(error)
	}
	});
	return request;
}

//get datas for graphic lines
function ajaxGraphicLines(url, container, dias)
{
	var request = $.ajax({
	url: url,
	type: 'post',
	beforeSend: function(){
		$(('#'+container)).html("");
		var target = document.getElementById(container);
		var spinner = new Spinner(opts).spin(target);
	},
	success: function(response){
		var variable = eval(response);
		drawGraphicLines(variable[0], variable[1], datesCurrentPeriod, container);
	},
	error: function(jqXHR, status, error){
		console.log(status)
		console.log(error)
	}
	});
	return request;
}

//draw graphics lines
function drawGraphicLines(last,current, dates, container)
{
	google.load('visualization', '1', {packages: ['line']});
	var oppCurrent=current;
    var dateByGraphic=dates;
    var oppLast= last;
    
   for(var i=0; i<=oppCurrent.length - 1; i++)
    {
      if(dateByGraphic[i]==null)
      {
        dateByGraphic[i]="";
      }
    }
      varTable = new Array();
      varTable[0] = ["", "Current Period", "Previous Period"];
      for(var i=0; i<= oppLast.length - 1 ; i++)
      { 
        var punto = new Array(dateByGraphic[i], ((oppCurrent[i]*1)), ((oppLast[i]*1)));
        varTable[i+1] = punto;
      }

      var data = new google.visualization.arrayToDataTable(varTable);
      var options = {
          colors:['#78b4cf', '#f3b49f'],
          legend:{position: "none"}, 
          vAxis: {title: 'Minutes', titleTextStyle: {color: 'black'}},
          chartArea:{width:"80%", height:"80%"},

        };
      google.load('visualization', '1', {packages: ['line']});  
      var chart = new google.charts.Line(document.getElementById(container));
	  chart.draw(data, options);
}

//draw avarage duration
function dibujarAvarageDuration( last, current, dates, container, typeGraphic)
{
	google.load("visualization", "1", {packages:[typeGraphic]});
    varTable = new Array();
    varTable[0] = ['Semana',  'Previous Period', 'Current Period'];
    for(var i=0; i<= dates.length - 1 ;i++)
    { 
      	var point = new Array(dates[i], last[i]*1, current[i]*1);
        varTable[i+1] = point;
    }

    var data = new google.visualization.arrayToDataTable(varTable);

    var options = {
          colors:[ '#f3b49f','#78b4cf'],
          legend:{position: "top"}, 
          vAxis: {title: 'Minutes', titleTextStyle: {color: 'black'}},
          chartArea:{width:"80%", height:"80%"},   
    };
    var chart = new google.visualization.ColumnChart(document.getElementById(container));
    chart.draw(data, options);     
}

//load bloques for dashboard
function loadBloque(url, container)
{
	var request = $.ajax({
	url: url,
	type: 'post',
	beforeSend: function(){
		if(container == "repetCustomers"){
			loadNews();
		}
			$(('#'+container)).html("");
			var target = document.getElementById(container);
			var spinner = new Spinner(opts).spin(target);
		
	},
	success: function(response){
		console.log("procesoterminado "+ container);
		if(container == "repetCustomers"){
			var variable = eval(response);
			drawNewAndRepetCustomers(variable[0],variable[1],container);	
			}
		else
			$(("#"+container)).html(response);
	},
	error: function(jqXHR, status, error){
		console.log(status)
		console.log(error)
	}
	});
	return request;
}

//load news
function loadNews()
{
	$(('#newCustomers')).html("");
			var target = document.getElementById('newCustomers');
			var spinner = new Spinner(opts).spin(target);
}

//draws and repeat customers
function drawNewAndRepetCustomers(result1, result2, container)
{
	if(result1>result2){
		var aux = result1 - result2;
		if(aux > 0)
			aux =((aux/result1)*100);
		else
			aux = 100;
		var repeat = Number(Math.round(aux+'e2')+'e-2');
		var repeatDates = repeat+"%<img src='../img/up.jpg' border='0'>";
		var newDates = repeat+"%<img src='../img/down.png' border='0'>";
	}else{
		var aux = result2 - result1;
		if(aux>0)
			aux =((aux/result2)*100);
		else
			aux = 100;
		var repeat = Number(Math.round(aux+'e2')+'e-2');
		var repeatDates = repeat+"%<img src='../img/down.png' border='0'>";
		var newDates = repeat+"%<img src='../img/up.jpg' border='0'>";
	}
		
	$(("#"+container)).html("<div id='yesterday'>Selected Period</div>"+
								"<div id='datos'>"+Number(Math.round(result1+'e2')+'e-2')+"%</div>"+
								"<div id='previus'>vs. Previous Period</div>"+
								"<div id='flechita'>"+repeatDates+"</div>");

	$(("#newCustomers")).html("<div id='yesterday'>Selected Period</div>"+
								"<div id='datos'>"+Number(Math.round((100-result1)+'e2')+'e-2')+"%</div>"+
								"<div id='previus'>vs. Previous Period</div>"+
								"<div id='flechita'>"+newDates+"</div>");
}


