$(document).ready(function(){
    fullLocations();
    
});

//change cheched All Location
function changeAllLocation()
{
    var checkedLocation = document.getElementById('idAllLocations').checked;
    var stores = document.getElementsByName("nameLocation");
    for(var i = 0; i < stores.length; i++)
        stores[i].checked = checkedLocation;
}

//change cheched All kPIs
function changeAllKpis()
{
    var checkedLocation = document.getElementById('idSelectKpis').checked;
    var stores = document.getElementsByName("nameKpi");
    for(var i = 0; i < stores.length; i++)
        stores[i].checked = checkedLocation;
}

//fill location
function fullLocations()
{
    var names = getStoresNames(); 
    var container = document.getElementById('idLocations');
    for (var i = 0; i < names.length ; i++) {
        var div = document.createElement( 'div' );
        div.className = "checkbox";
        div.innerHTML = "<label><input name='nameLocation' type='checkbox' value='"+names[i][0]+"'>"+names[i][1]+"</label>"; 
        container.appendChild(div);
    }    
}

//get stores names
function getStoresNames()
{
    var parametros = {
         "function" : "getStoresNames"
    };            
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/store.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
            tmp = eval(request);
        },
        error: function(jqXHR, status, error){
            console.log(status)
            console.log(error)
        }
    });
    return tmp;
    }();
    return return_first;  
}

//download csv Report
function downloadCsvReport()
{   
    var dateInit = new Date($("#fecha").datepicker("getDate"));
    var dateFinal = new Date($("#fecha2").datepicker("getDate"));
    dateFinal.setDate(dateFinal.getDate()+1);// this is for the query in the bd
    dateInit = changeFormatyyyymmdd(dateInit);
    dateFinal = changeFormatyyyymmdd(dateFinal);

    var typeReport = getTypeReport();
    var kpis = getKpis();
    var stores = getStores();
    var results = [];
    for(var i = 0; i < kpis.length; i++)
        results[i] = getResults(stores, dateInit, dateFinal, kpis[i]);
    downloadCsv(results);
    /*
    switch(typeReport) {
    case 'PerformanceReport':
        var datasReport = performanceReport();
        break;
    case 'ExportByHour':
        break;
    case 'ExportByDay':
        break;
    case 'ExportByWeek':
        break;  
    default:
        break;
    }
    */
}

//get result kpi
function getResultKpi(kpi)
{
    var res = '';
    switch(typeReport) {
    case 'averageDuration':
        res = performanceReport();
        break;
    case 'bounceRate':
        break;
    case 'customers':
        break;
    case 'newCustomers':
        break;
    case 'outsideConversion':
        break;  
    case 'outsideOpportunity':
        break;            
    case 'repeatCustomers':
        break;            
    default:
        break;
    }    
    return res;
}


//get type Report
function getTypeReport()
{
    var typeReport = document.getElementsByName("optionsRadios");
    for(var i = 0; i < typeReport.length; i++)
        if(typeReport[i].checked)
            return typeReport[i].value;
}

//get Kpis selected
function getKpis()
{
    var kpisSelected = [];
    var kpis = document.getElementsByName("nameKpi");
    for(var i = 0; i < kpis.length; i++)
        if(kpis[i].checked)
            kpisSelected[i] = kpis[i].value; 
    return kpisSelected;          
}

//get Stores selected
function getStores()
{
    var storesSelected = [];
    var stores = document.getElementsByName("nameLocation");
    for(var i = 0; i < stores.length; i++)
        if(stores[i].checked)
            storesSelected[i] = stores[i].value; 
    return storesSelected;          
}

//Performance Report
function performanceReport()
{

}

//change format date yyyy-mm-dd
function changeFormatyyyymmdd(date)
{   
    res = (date.getFullYear() + '-' + (date.getMonth()+1) + '-' +  date.getDate());
    return res;
}

//get results comapare
function getResults(listStores, dateInit, dateFinal, functionName)
{
     var parametros = {
        "storesChecked" : listStores,
        "dateInit" : dateInit,
        "dateFinal" : dateFinal,
        "function" : functionName
    };            
    var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/compare.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
            tmp = eval(request);
        },
        error: function(jqXHR, status, error){
            console.log(status)
            console.log(error)
        }
    });
    return tmp;
    }();
    return return_first;     
}

function downloadCsv(data)
{
    //var data = [["name1", "city1", "some other info"], ["name2", "city2", "more info"]];
    var csvContent = "data:text/csv;charset=utf-8,";
    data.forEach(function(infoArray, index){

       dataString = infoArray.join(",");
       csvContent += index < data.length ? dataString+ "\n" : dataString;

    }); 
    var encodedUri = encodeURI(csvContent);
    window.open(encodedUri);
    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "my_data.csv");

    link.click(); // This will download the data file named "my_data.csv".
}