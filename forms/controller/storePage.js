var idStore = null;

$(document).ready(function(){
	if(readCookie('admin') == "admin")
		ShowAdminLayout();
	idStore = readCookie('idStore');
	completeStore(idStore);
	completeArrayZones(idStore); 
})

//complete store form
function completeStore(idStore)
{
	var store = getStore(idStore);
	document.getElementById("name").value = store[1];
	document.getElementById("email").value = store[6];
	document.getElementById("address").value = store[2];
	document.getElementById("responsible").value = store[4];
	document.getElementById("geo").value = store[3];
	document.getElementById("telephone").value = store[5];
}

//get store from BD
function getStore(idStore)
{
	var parametros = {
					"idStore": idStore,
					"function" : "getStore"
				};
				
	var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/store.php',
        'data': parametros,
        beforeSend: function(){
		},
        'success': function (request) {
         	tmp = eval(request);
        }
    });
    return tmp;
	}();
	return return_first;
}

//save store in BD
function saveStore()
{
	var email = document.getElementById("email").value;
	var name = document.getElementById("name").value;
	var address = document.getElementById("address").value;
	var telephone = document.getElementById("telephone").value;
	var responsible = document.getElementById("responsible").value;
	var geo = document.getElementById("geo").value;
	if(idStore == null)
		insertarNewStore(email, name, address, telephone, responsible, geo);
	else
		if(name != ""){
			updateStore(email, name, address, telephone, responsible, geo);
			putClassInDiv("divNameStore", "form-group");
			putMessageInElementSucces("message", "div", "alert-success form-group", "Updated");	
		}else
			putMessageInElement("message", "divNameStore", "The name is empty", "has-error form-group");
}

//update store in BD
function updateStore(email, name, address, telephone, responsible, geo)
{
	var parametros = {
					"email": email,
					"responsible": responsible,
					"name": name,
					"address": address,
					"geo": geo,
					"telephone": telephone,
					"idStore": idStore,
					"function": "updateStore"
				};
	$.ajax({
					data: parametros,
					url: '../model/store.php',
					type: 'post',
					beforeSend: function(){
						
					},
					success: function(response){
					},
					error: function(jqXHR, estado, error){
						console.log(estado)
						console.log(error)
					}
				});

}

//load page
function loadFormSensor()
{
	$("#subContainer").load("../store/editStore.html");
}

//complete array zones
function completeArrayZones(idStore)
{
	var zones = getZonesArray(idStore);
	for(var i = 0; i < zones.length; i++)
	{
		insertItemInTableZone(zones[i]);
	}
}

//get zones from array
function getZonesArray(idStore)
{
	var parametros = {
					"idStore": idStore
				};
				
	var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/zones.php',
        'data': parametros,
        beforeSend: function(){
        },
        'success': function (request) {
         	tmp = eval(request);
        }
    });
    return tmp;
	}();
	return return_first;
}

//insertt item in zones table
function insertItemInTableZone(zone)
{
	var table = document.getElementById("idTableZones");
    var row = table.insertRow(1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var idZoneText = "idZone"; 
    cell1.innerHTML = '<a href= "../pages/zonePage.html" onclick="saveInSession('+zone[0]+')">'+zone[1]+'</a>';
    cell2.innerHTML = '<button type="submit" class="btn btn-danger" onclick="deleteZone('+zone[0]+')">Delete</button>';     
}

//save zone in session
function saveInSession(idZone)
{
	writeCookie('idZone', idZone, 1);
}

//save zone in BD
function saveZone()
{
	var name = document.getElementById("nameZone").value;
	if(name != ""){
		insertNewZoneInBD(name, idStore);
		updatePage();
	}else
		putMessageInElementStore("messageZone", "nameZone", "The name is empty", "has-error form-control");
}

//insert zone in BD
function insertNewZoneInBD(name, idStore)
{ 
	var parametros = {
					"name": name,
					"store": idStore
				};
	$.ajax({
					data: parametros,
					url: '../model/zone.php',
					type: 'post',
					async: false,
					beforeSend: function(){
					},
					success: function(response){
					},
					error: function(jqXHR, estado, error){
						console.log(estado)
						console.log(error)
					}
				});
}

//delete zone in BD
function deleteZone(idZone)
{
	var textConfirmation = "are you sure? you will delete all the Zone's elements ";

	if(confirm(textConfirmation))
	{
	    var parametros = {
	                    "idZone": idZone,
	                    "function" : "deleteZone"
	                };
	                
	    var return_first = function () {
	    var tmp = null;
	    $.ajax({
	        'async': false,
	        'type': "POST",
	        'global': false,
	        'dataType': 'html',
	        'url': '../model/zone.php',
	        'data': parametros,
	        beforeSend: function(){
	        },
	        'success': function (request) {
	            tmp = eval(request);
	           updatePage();
	        }
	    });
	    return tmp;
	    }();
	    return return_first;
	}
}

//update page
function updatePage()
{
	window.location.href = "storePage.html";
}