$(document).ready(function () {
    $("#captchaCode").keyup(function(event){
        if(event.keyCode == 13){
            signUp();
        }
    });
});

//validation sing up
function signUp()
{
	var email = document.getElementById("emailSignUp");
	var pass = document.getElementById("passwordSignUp");
	var repetPass = document.getElementById("repetPasswordSignUp");
	var captchaCode = document.getElementById("captchaCode");
	
		if(validEmail(email.value)){
			putClassInDiv("divEmail", "form-group");
			if(validPass(pass, repetPass)){
				if((!existMail(email.value))&&(!existEmailAdmin(email.value))){
					putClassInDiv("divEmail", "form-group");
					if(correctCaptcha(captchaCode.value))
					{
						putClassInDiv("divCaptchaCode", "form-group");
						var idCustomer = insertNewCustomer(email.value, pass.value);
						if( idCustomer != 0){
							putMessageInElementSucces("message", "div", "alert alert-success form-group", "Success creation, you will receive an email to register your account");
						}else
							putMessageInElement("message", "", "An unexpected problem occurred", "has-error form-group");
					}else{
						putMessageInElement("message", "divCaptchaCode", "the captcha is invalid", "has-error form-group");
						document.getElementById("captcha").src = "../../captcha/securimage_show.php";
					}
				}else
					putMessageInElement("message", "divEmail", "email already exist", "has-error form-group");
			}
		}else
			putMessageInElement("message", "divEmail" ,"email is invalid, example: email@exaple.com", "has-error form-group");
		
		
}

//correct catcha?
function correctCaptcha(captchaCode)
{
	var parametros = {
					"captchaCode": captchaCode,
					"function" : "correctCaptcha"
				};
				
	var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/signUp.php',
        'data': parametros,
        beforeSend: function(){
		 	//alert("antes de mandar");
		},
        'success': function (request) {
         	tmp = eval(request);
        }
    });
    return tmp;
	}();
	return return_first;
}

//insert new customer
function insertNewCustomer(email, pass)
{
	var parametros = {
					"email": email,
					"pass": pass,
					"function" : "insertNewCustomer"
				};
				
	var return_first = function () {
    var tmp = null;
    $.ajax({
        'async': false,
        'type': "POST",
        'global': false,
        'dataType': 'html',
        'url': '../model/signUp.php',
        'data': parametros,
        beforeSend: function(){
		},
        'success': function (request) {
         	tmp = eval(request);
         	//alert(tmp);
        }
    });
    return tmp;
	}();
	return return_first;
}
