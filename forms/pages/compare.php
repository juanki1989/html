<?php
	session_start();
	if(empty($_SESSION))
	header("Location: ../../index.html");

?>
<html>
	<head>
		<link type='text/css' rel='stylesheet' href='../css/estilo.css' />
		<link rel="icon" type="image/png" href="../img/favicons.png" />
		<meta http-equiv="Content-type" content="text/html; charset=UTF-8"/>
		
		 <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

	    <!-- MetisMenu CSS -->
	    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

	    <!-- Morris Charts CSS -->
	    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

	    <!-- Custom Fonts -->
	    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	   <script type="text/javascript" src="../js/spin.js"></script>
	   <script LANGUAJE="javascript">
		function ir(loc)
			{
				window.location.href =  loc.options[loc.selectedIndex].value;
			}
		</script>
	</head>

	
<div id="contenedor-mains">
	
	<div class="menu">
	<div id="logo"><img src="../img/logo.jpg"></img></div>	
	<div id="text"><a href="dashboardCustomer.php">DASHBOARD</a></div>
	<div id="text"><a href="locations.php">LOCATIONS</a></div>
	<div id="text"><font color="#78b4cf">COMPARE</font></div>
	<div id="text"><a href="reports.php">REPORTS</a></div>
	<div id="selloc">
		<select class="formulario" name="loc" onChange="ir(this)">
			<option value="0">
				<?php 
					if(empty($_SESSION))
						session_start();
					$id_user=$_SESSION['usuario'];
					include_once("../model/conexion.php");
					$sql= "SELECT responsible from customer where customer_code='$id_user'";
					$result=mysql_query($sql);
					$dato=mysql_fetch_row($result);
					echo $dato[0];
				?>
			</option>
			<option value="changePassword.html">Change Password</a></option>
			<option value="../model/cerrarSesion.php">Cerrar Sesión</a>
			</option>
		</select>
	</div>
	</div>		
	
	<div class="barra">
		<div class="nombre">Compare Locations</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="col-lg-8">
					<div class="col-md-12 col-md-offset-1">
			            <div class="panel panel-default">
			                <div class="panel-body">   
			                	<label>Choose Time Period</label>
								<input onchange='showCompare()' type='text' class="formulario" name='fecha' id='fecha' value="<?php echo $_SESSION['fechaPresetecion1']; ?>" readonly/> 
  								<input onchange='showCompare()' type='text' class="formulario" name='fecha2' id='fecha2' value="<?php echo $_SESSION['fechaPresetecion2']; ?>" readonly/>
							</div>
			                <!-- /.panel-body -->
			            </div>
			            <!-- /.panel -->
			         </div>
					<div class="col-md-12 col-md-offset-1">
			            <div class="panel panel-default">
			                <div class="panel-heading">
			                    <label>Compare</label>
			                </div>
			                <!-- /.panel-heading -->
			                <div class="panel-body">
			                    <div id="morris-bar-chart"></div>
			                </div>
			                <!-- /.panel-body -->
			            </div>
			            <!-- /.panel -->
			        </div>
				</div>
				<div class="col-lg-4">
					<div class="col-md-8 col-md-offset-2">
						<div class="form-group">
							<label>Choose KPI</label>
							<select class="form-control" id="idKpi" onchange='showCompare()'>
	                            <option>Customers</option>
	                            <option>Outside Conversion</option>
	                            <option>Outside Opportunity</option>
	                            <option>Avarage Duration</option>
	                            <option>Bounce Rate</option>
	                            <option>News Customers</option>
	                            <option>Repeat Customers</option>
	                        </select>
	                    </div>    
			            <div class="panel panel-default">
			                <div class="panel-heading">
			                    <label>Benchmarks</label>
			                </div>
			                <!-- /.panel-heading -->
			                <div class="panel-body">
			                    <div class="checkbox">
	                                <label>
	                                    <input id='idAllLocations' onchange='changeAllLocation()' type="checkbox" value="">All Locations
	                                </label>
	                            </div>
			                </div>
			                <!-- /.panel-body -->
			            </div>
			            <!-- /.panel -->
			            <div class="panel panel-default">
			                <div class="panel-heading">
			                    <label>Locations</label>
			                </div>
			                <!-- /.panel-heading -->
			                <div class="panel-body" id="idLocations">
			                    
			                </div>
			                <!-- /.panel-body -->
			            </div>
			            <!-- /.panel -->
			        </div>
				</div>
			</div>
			   
		</div>
	</div>
</div>
		

	<script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
    <script src="../js/jquery.js"></script>
	<script src="../js/jquery-ui.js"></script>
	<script src="../controller/functionsCompare.js"></script>
	<script LANGUAJE="javascript">
    	var minDateVar2 = '<?php include("../model/minDate.php"); ?>';
        var daysToAdd = -1;
        $("#fecha").datepicker({
        dateFormat: "dd/mm/yy",
        maxDate: -1,
        minDate: minDateVar2,
        onSelect: function (selected) {
        	showCompare();
            var dtMax = new Date(selected);
            dtMax.setDate(dtMax.getDate()); 
            var dd = dtMax.getDate();
            var mm = dtMax.getMonth();
            var y = dtMax.getFullYear();
            var dtFormatted = dd + '/'+ mm + '/'+ y;
            $("#fecha2").datepicker("option", "dateFormat", "dd/mm/yy");
            $("#fecha2").datepicker("option", "minDate", selected);
        }
	    });

	    $("#fecha2").datepicker({
	        dateFormat: "dd/mm/yy",
	        maxDate: -1,
	        minDate: $("#fecha").datepicker({dateFormat: 'dd/mm/yy'}).val()
	    });
    </script>
</html>
