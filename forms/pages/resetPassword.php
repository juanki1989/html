<?php
    if(empty($_GET['code']) || empty($_GET['email']))
        header("Location: ../../index.html");
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>B'Wireless Control Panel</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="icon" type="image/png" href="../img/favicons.png" />
    
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Reset Password</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group row text-center">
                            <img src="../img/logo.jpg" alt="">
                        </div>
                        <fieldset>
                            <div class="form-group" id="message">
                            </div>
                            <div class="form-group" id="divPassword">
                                <label>New Password</label>
                                <input class="form-control" id="password"value placeholder="New Password" name="newPassword" type="password" value="" required>
                            </div>
                            <div class="form-group" id="divPasswordRepeat">
                                <label>Repeat New Password</label>
                                <input class="form-control" id="passwordRepeat"value placeholder="Repeat New Password" name="repeatNewPassword" type="password" value="" required>
                            </div>
                            <?php
                                $code = $_GET['code'];
                                $email = $_GET['email'];
                                $function = "resetPasswordCustomer(".$code.",'".$email."')";
                                echo "<button id='send' type='submit' class='btn btn-lg btn-primary btn-block' onclick=".$function.">Save</button>";
                            ?>
                        </fieldset>    
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <script src="../controller/functionGlobals.js"></script>

     <script src="../controller/login.js"></script>
</body>

</html>
