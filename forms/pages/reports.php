<?php
	session_start();
	if(empty($_SESSION))
	header("Location: ../../index.html");

?>
<html>
	<head>
		<link type='text/css' rel='stylesheet' href='../css/estilo.css' />
		<link rel="icon" type="image/png" href="../img/favicons.png" />
		<meta http-equiv="Content-type" content="text/html; charset=UTF-8"/>
		<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

	    <!-- MetisMenu CSS -->
	    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

	    <!-- Morris Charts CSS -->
	    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

	    <!-- Custom Fonts -->
	    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<script LANGUAJE="javascript">
			function ir(loc)
			{
				window.location.href =  loc.options[loc.selectedIndex].value;
			}
		</script>
	</head>


	<div id="contenedor-mains">
	
		<div class="menu">
		<div id="logo"><img src="../img/logo.jpg"></img></div>	
		<div id="text"><a href="dashboardCustomer.php">DASHBOARD</a></div>
		<div id="text"><a href="locations.php">LOCATIONS</a></div>
		<div id="text"><a href="compare.php">COMPARE</a></div>
		<div id="text"><font color="#78b4cf">REPORTS</font></div>
		<div id="selloc">
			<select class="formulario" name="loc" onChange="ir(this)">
				<option value="0">
					<?php 
						if(empty($_SESSION))
							session_start();
						$id_user=$_SESSION['usuario'];
						//echo $id_user;
						include_once("../model/conexion.php");
						$sql= "SELECT responsible from customer where customer_code='$id_user'";
						$result=mysql_query($sql);
						$dato=mysql_fetch_row($result);
						echo $dato[0];
					?>
				</option>
				<option value="hangePassword.html">Change Password</a></option>
				<option value="../model/cerrarSesion.php">Cerrar Sesión</a>
				</option>
			</select>
		</div>
		</div>		
	
	<div class="barra">
		<div class="nombre">Custom Report</div>
	</div>
		
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="col-md-6 col-md-offset-5">
						<button id='idDownloadCsvReport' onclick='downloadCsvReport()' type="button" class="btn btn-primary btn-lg">Download CSV</button>
					</div>
				</div>
			</div>	
			<div class="row">
				<div class="col-lg-12">
					<div class="col-md-12 col-md-offset-1">
						<h4>1. Select a spreadsheet type</h4>
					</div>
				</div>
			</div>	
			<div class="radio" id='idSetChecks'>
				<div class="row">
					<div class="col-lg-12">
						<div class="col-lg-6">
							<div class="col-md-offset-2">
								<div class="form-group">
		                            <label>
		                                <input type="radio" name="optionsRadios" id="optionsRadiosPerformanceReport" value="PerformanceReport" checked="">Performance Report</input>
		                            	<p><small>Export a csv of your locations where each KPI cell represents <br>a summed or averaged value for a given time period</small></p>
		                            	<img src="../img/Performance Report.png" class="img-responsive img-rounded" alt="Cinque Terre">
		                            </label>
		                        </div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>
	                                <input type="radio" name="optionsRadios" id="optionsRadiosExportByHour" value="ExportByHour">Export by Hour</input>
	                                <p><small>Export a csv of your locations where each KPI cell represents the value<br> for a given hour of the day</small></p>
	                                <img src="../img/Performance Report.png" class="img-responsive img-rounded" alt="Cinque Terre">
	                            </label>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="col-lg-6">
							<div class="col-md-offset-2">
								<div class="form-group">
		                            <label>
		                                <input type="radio" name="optionsRadios" id="optionsRadiosExportByDay" value="ExportByDay">Export by Day</input>
		                                <p><small>Export a csv of your locations where each KPI cell represents the value<br> for a given day</small></p>
		                                <img src="../img/Performance Report.png" class="img-responsive img-rounded" alt="Cinque Terre">
		                            </label>
	                            </div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label>	
	                                <input type="radio" name="optionsRadios" id="optionsRadiosExportByWeek" value="ExportByWeek">Export by Week</input>
	                                <p><small>Export a csv of your locations where each KPI cell represents the value for a<br> given week</small></p>
	                                <img src="../img/Performance Report.png" class="img-responsive img-rounded" alt="Cinque Terre">
	                            </label>
                            </div>
						</div>
					</div>
				</div>
			</div>
			<hr class="utils-margin--none">
			<div class="row">
				<div class="col-lg-12">
					<div class="col-md-12 col-md-offset-1">
						<h4>2. Select date range</h4>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="col-md-12 col-md-offset-2">
						<input onchange='showCompare()' type='text' class="formulario" name='fecha' id='fecha' value="<?php echo $_SESSION['fechaPresetecion1']; ?>" readonly/> 
  						<input onchange='showCompare()' type='text' class="formulario" name='fecha2' id='fecha2' value="<?php echo $_SESSION['fechaPresetecion2']; ?>" readonly/>
					</div>
				</div>
			</div>
			<hr class="utils-margin--none">
			<div class="row">
				<div class="col-lg-6">
					<div class="col-md-10 col-md-offset-2">
						<h4>3. Select Location(s)</h4>
						<div class="col-md-0 col-md-offset-1">
							<div class="panel panel-default">
				                <div class="panel-heading">
				                    <input id='idAllLocations' onchange='changeAllLocation()' type="checkbox" value="">
				                    <label>All Locations</label>
				                </div>
				                <!-- /.panel-heading -->
				                <div class="panel-body" id="idLocations">
				                    
				                </div>
				                <!-- /.panel-body -->
				            </div>
			            </div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="col-md-10 col-md-offset-0">
						<h4>4. Select KPI(s)</h4>
						<div class="col-md-0 col-md-offset-1">
							<div class="panel panel-default">
				                <div class="panel-heading">
				                    <input id='idSelectKpis' onchange='changeAllKpis()' type="checkbox" value="">
				                    <label>All KPI(s)</label>
				                </div>
				                <!-- /.panel-heading -->
				                <div class="panel-body">
				                	<div class="checkbox"><label><input name="nameKpi" type="checkbox" value="compareAvarageDuration">Average Duration</label></div>
									<div class="checkbox"><label><input name="nameKpi" type="checkbox" value="compareBounceRate">Bounce Rate</label></div>
									<div class="checkbox"><label><input name="nameKpi" type="checkbox" value="compareCustomers">Customers</label></div>
									<div class="checkbox"><label><input name="nameKpi" type="checkbox" value="compareNewCustomers">New Customers</label></div>
									<div class="checkbox"><label><input name="nameKpi" type="checkbox" value="compareOutsideConversion">Outside Conversion</label></div>
									<div class="checkbox"><label><input name="nameKpi" type="checkbox" value="compareOutsideOpportunity">Outside Opportunity</label></div>
									<div class="checkbox"><label><input name="nameKpi" type="checkbox" value="compareRepeatCustomers">Repeat Customers</label></div>
				                </div>
				                <!-- /.panel-body -->
				            </div>
			            </div>
					</div>
				</div>
			</div>
		</div>				
	</div>					
	<script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
    <script src="../js/jquery.js"></script>
	<script src="../js/jquery-ui.js"></script>
	<script src="../controller/functionsReports.js"></script>
	<script LANGUAJE="javascript">
    	var minDateVar2 = '<?php include("../model/minDate.php"); ?>';
        var daysToAdd = -1;
        $("#fecha").datepicker({
        dateFormat: "dd/mm/yy",
        maxDate: -1,
        minDate: minDateVar2,
        onSelect: function (selected) {
        	showCompare();
            var dtMax = new Date(selected);
            dtMax.setDate(dtMax.getDate()); 
            var dd = dtMax.getDate();
            var mm = dtMax.getMonth();
            var y = dtMax.getFullYear();
            var dtFormatted = dd + '/'+ mm + '/'+ y;
            $("#fecha2").datepicker("option", "dateFormat", "dd/mm/yy");
            $("#fecha2").datepicker("option", "minDate", selected);
        }
	    });

	    $("#fecha2").datepicker({
	        dateFormat: "dd/mm/yy",
	        maxDate: -1,
	        minDate: $("#fecha").datepicker({dateFormat: 'dd/mm/yy'}).val()
	    });
    </script>
</html>
