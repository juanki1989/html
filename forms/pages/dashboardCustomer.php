<?php
	session_start();
	if(empty($_SESSION))
	header("Location: ../../index.html");
?>

<html>
	<head>
		<link type='text/css' rel='stylesheet' href='../css/estilo.css' />
		<link rel="icon" type="image/png" href="../img/favicons.png" />
		<meta http-equiv="Content-type" content="text/html; charset=UTF-8"/>

		<script LANGUAJE="javascript">
			function ir(loc)
			{
				window.location.href =  loc.options[loc.selectedIndex].value;
			}
			function ayuda1()
			{
				document.getElementById("v1").style.display="none";
				document.getElementById("info1").style.display="block";
			}
			function cerrar_ayuda1()
			{
				document.getElementById("info1").style.display="none";
				document.getElementById("v1").style.display="block";				
			}
			function ayuda2()
			{
				document.getElementById("v2").style.display="none";
				document.getElementById("info2").style.display="block";
			}
			function cerrar_ayuda2()
			{
				document.getElementById("info2").style.display="none";
				document.getElementById("v2").style.display="block";				
			}
			function ayuda3()
			{
				document.getElementById("v3").style.display="none";
				document.getElementById("info3").style.display="block";
			}
			function cerrar_ayuda3()
			{
				document.getElementById("info3").style.display="none";
				document.getElementById("v3").style.display="block";				
			}
			function ayuda4()
			{
				document.getElementById("v4").style.display="none";
				document.getElementById("info4").style.display="block";
			}
			function cerrar_ayuda4()
			{
				document.getElementById("info4").style.display="none";
				document.getElementById("v4").style.display="block";				
			}
				function ayuda5()
			{
				document.getElementById("v5").style.display="none";
				document.getElementById("info5").style.display="block";
			}
			function cerrar_ayuda5()
			{
				document.getElementById("info5").style.display="none";
				document.getElementById("v5").style.display="block";				
			}
				function ayuda6()
			{
				document.getElementById("v6").style.display="none";
				document.getElementById("info6").style.display="block";
			}
			function cerrar_ayuda6()
			{
				document.getElementById("info6").style.display="none";
				document.getElementById("v6").style.display="block";				
			}
				function ayuda7()
			{
				document.getElementById("v7").style.display="none";
				document.getElementById("info7").style.display="block";
			}
			function cerrar_ayuda7()
			{
				document.getElementById("info7").style.display="none";
				document.getElementById("v7").style.display="block";				
			}
				function ayuda8()
			{
				document.getElementById("v8").style.display="none";
				document.getElementById("info8").style.display="block";
			}
			function cerrar_ayuda8()
			{
				document.getElementById("info8").style.display="none";
				document.getElementById("v8").style.display="block";				
			}
				function ayuda9()
			{
				document.getElementById("v9").style.display="none";
				document.getElementById("info9").style.display="block";
			}
			function cerrar_ayuda9()
			{
				document.getElementById("info9").style.display="none";
				document.getElementById("v9").style.display="block";				
			}
		</script>
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
<script src="../js/jquery.js"></script>
<script src="../js/jquery-ui.js"></script>

<script type="text/javascript" src="../js/spin.js"></script>
	</head>
		
	<div id="contenedor-mains">
	
		<div class="menu">
		<div id="logo"><img src="../img/logo.jpg"></img></div>	
		<div id="text"><font color="#78b4cf">DASHBOARD</font></div>
		<div id="text"><a href="locations.php">LOCATIONS</a></div>
		<div id="text"><a href="compare.php">COMPARE</a></div>
		<div id="text"><a href="reports.php">REPORTS</a></div>
		
		<div id="selloc">
			<select class="formulario" name="loc" onChange="ir(this)">
				<option value="0">
					<?php 
						echo $_SESSION['responsible'];
					?>
				</option>
				<option value="changePassword.html">Change Password</a></option>
				<option value="../model/cerrarSesion.php">Close session</a></option>
			</select>
		</div>
	</div>		
	
	<div class="barra">
		<div class="nombre">Standard Board</div>
	</div>
	<div class="grupo">
		<div class="filtro">
			<div class="ff">
				Choose Time Period
				<div class="ali">
 					<form action="../model/actFiltros.php" method="post" >
						<input type='text' onchange='startProcess(false);' class="formulario" name='fecha' id='fecha' value="<?php echo $_SESSION['fechaPresetecion1']; //echo date('m/d/Y', strtotime("-1 day")); ?>" readonly /> 
  						<input type='text' onchange='startProcess(false);' class="formulario" name='fecha2' id='fecha2' value="<?php echo $_SESSION['fechaPresetecion2']; //echo date('m/d/Y', strtotime("-1 day")); ?>" readonly />
				</div>
			</div>
			<div class="ff">
				Comparison Period
				<div class="ali">
				<select class="formulario" id = "periods" name="periods" onchange='startProcess(false);'>
					<option selected value="0">Previous Period</option>
					<option value="1">Previous 4 Period Average</option>
					<option value="2">Year Over Year</option>
				</select>
					
			</div>
			</div>
			<div class="ff">
				Choose Location
				<div class="ali">
					<select class="formulario2" name="sela">
						<option value="0">Locations</option>

					</select>
					<select class="formulario3" id="locations" name="sel_locations" onchange='startProcess(false);'>
						<?php
							$id_user=$_SESSION['usuario'];		
							include("../model/conexion.php");
							$u="SELECT store_code, store_name from stores where customer_code='$id_user'";
							$resultado=mysql_query($u);
							$i=1;
							while($r= mysql_fetch_array($resultado))
							{
								$scode=$_SESSION['loc_id'];
								if($i==$scode)	
								{
									echo '<option value="' . $r['store_code'] . '" selected>' . $r['store_name'] . '</option>';
								}
								else
								{
									echo '<option value="' . $r['store_code'] . '">' . $r['store_name'] . '</option>';

								}
								$i++;
							}
							unset($i);
						?>
					</select>
					</form>
				</div>
			</div>
			<div></div>
		</div>
	
		<div id="info1"class="flotados_info1">
			<div id="cabcolor">OUTISIDE OPPORTUNITY<div id="info" OnClick="cerrar_ayuda1()">x</div></div>
				<div id="internal_text">
					Number of people with Wi-Fi enabled smartphones detected outside and inside your location
					<br/><br/>
					Quick view and compare of key performance indicator
				</div>	
		</div>
		<div id="v1" class="flotados">
			<div id="cabcolor">OUTISIDE OPPORTUNITY<div id="info" OnClick="ayuda1()">?</div></div>
			<div id="outsideOpportunity" class="div_contenedor2">
				
			</div>
			
		</div>

		<div id="info2" class="flotados_info1">
			<div id="cabcolor">OUTISIDE CONVERSION<div id="info" OnClick="cerrar_ayuda2()">x</div></div>
			<div id="internal_text">
					Number of customers who entered the location divided by Outside Opportunity
					<br/><br/>	
					Quick view and compare of key performance indicator
				</div>	
		</div>
		<div id="v2" class="flotados">
			<div id="cabcolor">OUTISIDE CONVERSION<div id="info" OnClick="ayuda2()">?</div></div>
			<div id="outsideConversion" class="div_contenedor2">
				
			</div>
			
		</div>

		<div id="info3" class="flotados_info1">
			<div id="cabcolor">AVERAGE DURATION<div id="info" OnClick="cerrar_ayuda3()">x</div></div>
			<div id="internal_text">
					Average minutes spent in your location
					<br/><br/>
					Quick view and compare of key performance indicator
				</div>	
		</div>
		<div id="v3" class="flotados">
			<div id="cabcolor">AVERAGE DURATION<div id="info" OnClick="ayuda3()">?</div></div>
			<div id="averageDuration" class="div_contenedor2">
				
			</div>
			
		</div>

		<div id="info4" class="flotados_info1">
			<div id="cabcolor">BOUNCE RATE<div id="info" OnClick="cerrar_ayuda4()">x</div></div>
			<div id="internal_text">
					Customers who spent less than X min (default 5min) in your location
					<br/><br/>
					Quick view and compare of key performance indicator
				</div>	
		</div>
		<div id="v4" class="flotados">
			<div id="cabcolor">BOUNCE RATE<div id="info" OnClick="ayuda4()">?</div></div>
			<div id="bounceRate" class="div_contenedor2">
				
			</div>
			
		</div>

		<div id="info5" class="flotados_info2">
			<div id="cabcolor2">OUTISIDE OPPORTUNITY<div id="info" OnClick="cerrar_ayuda5()">x</div></div>	
			<div id="internal_text2">
					Number of people with Wi-Fi enabled smartphones detected outside and inside your location 
					<br/><br/>
					Data chart across time 
				</div>
		</div>
		<div id="v5" class="flotados2">
			<div id="cabcolor2">OUTISIDE OPPORTUNITY<div id="info" OnClick="ayuda5()">?</div></div>
			<div id="opportunity_div"class="div_contenedor"></div>			
		</div>

		<div id="info6" class="flotados_info2">
			<div id="cabcolor2">OUTISIDE CONVERSION<div id="info" OnClick="cerrar_ayuda6()">x</div></div>	
			<div id="internal_text2">
					Number of Customers who entered the location divided by Outside Opportunity
					<br/><br/>	
					Data chart across time
				</div>
		</div>
		<div id="v6" class="flotados2">
			<div id="cabcolor2">OUTISIDE CONVERSION<div id="info" OnClick="ayuda6()">?</div></div>
			<div id="conv_div" class="div_contenedor"></div>
		</div>

		<div id="info7" class="flotados_info1">
			<div id="cabcolor">NEW CUSTOMERS<div id="info" OnClick="cerrar_ayuda7()">x</div></div>
			<div id="internal_text">
					People with Wi-Fi enabled smartphones who have never been detected by XXX sensors at your location
					<br/><br/>		
					Quick view and compare of key performance indicator
				</div>		
		</div>
		<div id="v7" class="flotados">
			<div id="cabcolor">NEW CUSTOMERS<div id="info" OnClick="ayuda7()">?</div></div>
			<div id="newCustomers" class="div_contenedor2">
				
			</div>
			
		</div>

		<div id="info8" class="flotados_info1">
			<div id="cabcolor">REPEAT CUSTOMERS<div id="info" OnClick="cerrar_ayuda8()">x</div></div>	
			<div id="internal_text">
					People with Wi-Fi enabled smartphones who have already been detected by B'Wireless sensors at your location
					<br/><br/>
					Quick view and compare of key performance indicator
				</div>	
		</div>

		<div id="v8" class="flotados">
			<div id="cabcolor">REPEAT CUSTOMERS<div id="info" OnClick="ayuda8()">?</div></div>
			<div id="repetCustomers" class="div_contenedor2">
				
			</div>
			
		</div>

		<div id="info9" class="flotados_info2">
			<div id="cabcolor2">AVERAGE DURATION<div id="info" OnClick="cerrar_ayuda9()">x</div></div>	
			<div id="internal_text2">
					Average minutes spent in your location
					<br/><br/>
					Data chart for days of the week
				</div>
		</div>
		<div id="v9" class="flotados2">
			<div id="cabcolor2">AVERAGE DURATION<div id="info" OnClick="ayuda9()">?</div>
		</div>
			<div id="avg_div" class="div_contenedor"></div>
			
		</div>
		<div class="clear"></div>
	</div>
	</div>
	<script type="text/javascript" src="https://www.google.com/jsapi">
		
	</script>
	<script type="text/javascript">
    	google.load('visualization', '1', {packages: ['line']});
    	google.load("visualization", "1", {packages:["corechart"]});
  </script>
<script type="text/javascript" src="../controller/functionsDashBoard.js"></script>
</html>

