<?php

$arrayDates=opDates();
$date1=$arrayDates[0];
$date2=$arrayDates[1];
$diffDates=(int)opDays($date1, $date2);

if($_SESSION['periods'] == 0)
{
	$opResult1 = opPeriodsAgo('0', $diffDates, $date1, $date2,'0');
	$opResult2 = opPeriodsAgo('1', $diffDates, $date1, $date2, opCalculateDaysAgo($diffDates));
	opResfinal($opResult1, $opResult2);
}else{
	if($_SESSION['periods'] == 1)
	{
		getAvarageByPeriods('4', $diffDates, $date1, $date2);	
	}else{
		if($_SESSION['periods'] == 2)
		{
			opYearOverYear($diffDates, $date1, $date2);	
		}
	}	
}

//calculate days initial and date end
function opDates()
{
	if(empty($_SESSION))
		session_start();
	$become=$_SESSION['f'];
	$divide=explode('/', $become);
	$mounth=$divide[0];
	$day1=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $day1;
	unset($divide); unset($year);
	$d1=date("Y-m-d", strtotime($date));
	$become=$_SESSION['f2'];
	$divide=explode('/', $become);
	$mounth=$divide[0];
	$dia=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $dia;
	unset($divide); unset($mounth); unset($dia); unset($year);
	$d2=date("Y-m-d", strtotime($date));

	$range[0]=$d1;
	$range[1]=$d2;
	$range[2]=$day1;
	return $range;
}

//calculate days between initial date an final date
function opDays($date1, $date2)
{
	$days	= (strtotime($date2)-strtotime($date1))/86400;
	$days 	= abs($days); $days = floor($days);		
	return $days;
}

//calculate periods ago
function opPeriodsAgo($cantPeriodos,$diffDates, $date1, $date2, $daysAgo)
{
	
	$dateStart = null;
	$dateEnd = null;	
	if($cantPeriodos > 0)
	{ 
		$days="-" . $daysAgo . " day";
		$dateStart = strtotime ( $days , strtotime ( $date1 ) ) ;
		$dateStart = date ( 'Y-m-d' , $dateStart );
		$daysToSum = "+" . $diffDates . " day";
		$dateEnd = strtotime ( $daysToSum , strtotime ( $dateStart ) );
		$dateEnd = date ( 'Y-m-d' , $dateEnd );
	}else{
		$dateStart = $date1;
		$dateEnd = $date2;
	}
	$daysToSum = "+ 1 day";
		$dateEnd = strtotime ( $daysToSum , strtotime ( $dateEnd ) );
		$dateEnd = date ( 'Y-m-d' , $dateEnd );
	include("../model/conexion.php");
	$cOut1=0;
	$idStore=$_SESSION['loc_id'];
	$sql="SELECT r.mac
		  FROM zones z, sensors s, radacct r
		  WHERE z.store_code = $idStore
		  AND z.zone_code = s.zone_code
		  AND s.sensor_code = r.sensor_code
		  AND r.acc_stop_time BETWEEN '$dateStart' AND '$dateEnd'
		  ";
	$result=mysql_query($sql);
	
	while($reg=mysql_fetch_array($result))
	{
		++$cOut1;
	}
	mysql_close($conexion);
	return $cOut1;
}

//draw final result
function opResfinal($opResult1, $opResult2)
{
	echo '<div id="yesterday">Selected Period</div>
			<div id="datos">'.$opResult1.'</div>
			<div id="previus">vs. Previous Period</div>
			<div id="flechita">';
	if($opResult1>$opResult2)//ha bajado el porcentaje
	{
		$aux = $opResult1 - $opResult2;
		if($opResult1 > 0)
			$res=(($aux/$opResult1)*100);
		else
			$res=0;
		$n=number_format($res, 1, '.', '');
		echo $n . "%  ";			
		echo "<img src='../img/up.jpg' border='0'>";
	}
	else
	{
		$aux = $opResult2 - $opResult1;
		if($opResult2 > 0)
			$res=(($aux/$opResult2)*100);
		else
			$res=0;
		$n=number_format($res, 1, '.', '');
		echo $n . "%  ";
		echo "<img src='../img/down.png' border='0'>";	
	}
	echo '</div>';
}

//calcualte days ago
function opCalculateDaysAgo($diffDates)
{
	$daysAgo = 0;
	if($diffDates>6){
		$daysAgo = (((round($diffDates/7))+1)*7);
	}else{
		$daysAgo = 7;
	}
	return $daysAgo;
}

//calculate avarage by pediod
function getAvarageByPeriods($periods, $diffDates, $date1, $date2)
{
	$daysAgo = opCalculateDaysAgo($diffDates);
	
	
	$opResult1 = opPeriodsAgo('0', $diffDates, $date1, $date2,'0');
	$total =0;
	$opResult2 = 0;
	
	if($periods > 1)
	{
		for ($i=1; $i <= $periods; $i++) { 
			$total += opPeriodsAgo($periods, $diffDates, $date1, $date2,$daysAgo*$i);
		}
		$opResult2 = $total/$periods; 
	}else{
		$opResult2=opPeriodsAgo($periods, $diffDates, $date1, $date2, $daysAgo);
	}
	opResfinal($opResult1, $opResult2);
}

//calculate year over year
function opYearOverYear($diffDates, $date_ini, $date_fin)
{
	$opResult1 = opPeriodsAgo('0', $diffDates, $date_ini, $date_fin,'0');
	$date_ini_anterior = opRestYears(1, $date_ini);
	$date_fin_anterior = opRestYears(1, $date_fin);
	
	$opResult2 = opPeriodsAgo('0', $diffDates, $date_ini_anterior, $date_fin_anterior,'0');
	opResfinal($opResult1, $opResult2);
}

//rest years
function opRestYears($cantidad, $date)
{
	$years="-" . $cantidad . " year";
	$date = strtotime ( $years , strtotime ( $date ) ) ;
	$date = date ( 'Y-m-d' , $date );
	return $date;
}


?>