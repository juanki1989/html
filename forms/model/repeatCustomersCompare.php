<?php

$repeatArrayDates=repeatDates();//array con fechas
$repeatDate1=$repeatArrayDates[0];
$repeatDate2=$repeatArrayDates[1];
$repeatDiffDates=(int)repeatDays($repeatDate1, $repeatDate2);//dias que separan dos fechas

if($_SESSION['periods'] == 0)
{
	 $repeatResult1 = repeatPeriodsAgo('0',$repeatDiffDates, $repeatDate1, $repeatDate2, '0');
	 $repeatResult2 = repeatPeriodsAgo('1',$repeatDiffDates, $repeatDate1, $repeatDate2, repeatCalculateDaysAgo($repeatDiffDates));
	 echo json_encode(repeatResfinal($repeatResult1, $repeatResult2));
	
}else{
	if($_SESSION['periods'] == 1)
	{
		echo json_encode(repeatGetAvarageByPeriods('4', $repeatDiffDates, $repeatDate1, $repeatDate2));	
	}else{
		if($_SESSION['periods'] == 2)
		{
			echo json_encode(repeatYearOverYear($repeatDiffDates, $repeatDate1, $repeatDate2));	
		}
	}	
}

//calcualate initial date an final date
function repeatDates()
{
	if(empty($_SESSION))
		session_start();
	$become=$_SESSION['f'];
	$divide=explode('/', $become);
	$mounth=$divide[0];
	$day1=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $day1;
	unset($divide); unset($year);
	$d1=date("Y-m-d", strtotime($date));
	$become=$_SESSION['f2'];
	$divide=explode('/', $become);
	$mounth=$divide[0];
	$dia=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $dia;
	unset($divide); unset($mounth); unset($dia); unset($year);
	$d2=date("Y-m-d", strtotime($date));

	$range[0]=$d1;
	$range[1]=$d2;
	$range[2]=$day1;
	return $range;
}

//calculate between dates
function repeatDays($repeatDate1, $repeatDate2)
{
	$dias	= (strtotime($repeatDate2)-strtotime($repeatDate1))/86400;
	$dias 	= abs($dias); $dias = floor($dias);		
	return $dias;
}

//get min date
function repeatGetMinDate()
{
	$idStore=$_SESSION['loc_id'];
		
	$sql2="SELECT r.acc_stop_time FROM radacct r, zones z, sensors s WHERE z.store_code = $idStore
								AND z.zone_code = s.zone_code
								AND s.sensor_code = r.sensor_code order by r.acc_stop_time asc limit 1";
	$result=mysql_query($sql2);
	$data=mysql_fetch_row($result);
	$date=$data[0];
	
	return date("Y-m-d",strtotime($date));
}

//calculate avarage repets users
function repeatGetAvarage($dateStart,$dayFinal)
{
	include("../model/conexion.php");
	$total = 0;
	$resFinal = 0;
	$DateIniDataBase = repeatGetMinDate();
	$dayFinalDataBase = $dateStart;
	$idStore=$_SESSION['loc_id'];
	
	$sql=" SELECT COUNT( MACS.mac )
							FROM (	SELECT r.mac
									FROM zones z, sensors s, radacct r
									WHERE z.store_code = $idStore
									AND z.zone_code = s.zone_code
									AND s.sensor_code = r.sensor_code
									AND r.acc_start_time BETWEEN '$dateStart'
									AND '$dayFinal'
									GROUP BY r.mac
							) AS MACS, (	SELECT r.mac
											FROM zones z, sensors s, radacct r
											WHERE z.store_code = $idStore
											AND z.zone_code = s.zone_code
											AND s.sensor_code = r.sensor_code
											AND r.acc_start_time
											BETWEEN '$DateIniDataBase'
											AND '$dayFinalDataBase'
											GROUP BY r.mac
										) AS MACSBD
					WHERE MACS.mac = MACSBD.mac 
					";

	$result = mysql_query($sql);
	$res = mysql_fetch_row($result);
	
	$cantRepets = $res[0];

	$sql2 = "SELECT count( MACS.mac )
			FROM (

			SELECT r.mac
			FROM zones z, sensors s, radacct r
		    WHERE z.store_code = $idStore
		    AND z.zone_code = s.zone_code
		    AND s.sensor_code = r.sensor_code
			
			GROUP BY r.mac
			) AS MACS
			";
	$result2 = mysql_query($sql2);
	$res2 = mysql_fetch_row($result2);
	$cantTotalBd = $res2[0];
	$percentageRepets = ($cantRepets/$cantTotalBd)*100;
	mysql_close($conexion);
	return $percentageRepets;
}

//calculate periods ago
function repeatPeriodsAgo($cantPeriodos,$diffDates, $date1, $date2, $daysAgo)
{
	//echo $da
	$dateStart = null;
	$dayFinal = null;	
	if($cantPeriodos > 0)
	{ 
		$dias="-" . $daysAgo . " day";
		$dateStart = strtotime ( $dias , strtotime ( $date1 ) ) ;
		$dateStart = date ( 'Y-m-d' , $dateStart );
		$daysToSum = "+" . $diffDates . " day";
		$dayFinal = strtotime ( $daysToSum , strtotime ( $dateStart ) );
		$dayFinal = date ( 'Y-m-d' , $dayFinal );
	}else{
		$dateStart = $date1;
		$dayFinal = $date2;
	}
	//=========== esto es para tomar en cuenta la fecha hasta 
	$daysToSum = "+ 1 day";
		$dayFinal = strtotime ( $daysToSum , strtotime ( $dayFinal ) );
		$dayFinal = date ( 'Y-m-d' , $dayFinal );
	///=======
	return repeatGetAvarage($dateStart,$dayFinal);
}

//calculate days ago
function repeatCalculateDaysAgo($diffDates)
{
	$daysAgo = 0;
	if($diffDates>6){
		$daysAgo = (((round($diffDates/7))+1)*7);
	}else{
		$daysAgo = 7;
	}
	return $daysAgo;
}

//calcualte avarage bye periods
function repeatGetAvarageByPeriods($periods, $diffDates, $date1, $date2)
{
	$daysAgo = repeatCalculateDaysAgo($diffDates);
	$opResult1 = repeatPeriodsAgo('0',$diffDates, $date1, $date2, '0');
	$total =0;
	$opResult2 = 0;
	
	if($periods > 1)
	{
		for ($i=1; $i <= $periods; $i++) { 
			$total += repeatPeriodsAgo($periods, $diffDates, $date1, $date2,$daysAgo*$i);
		}
		$opResult2 = $total/$periods; 
	}else{
		$opResult2 = repeatPeriodsAgo($periods, $diffDates, $date1, $date2, $daysAgo);
	}
	return repeatResfinal($opResult1, $opResult2);
	
}

//draw final result
function repeatResfinal($repeatResult1, $repeatResult2)
{
	$resultado = null;
	$resultado[0] = $repeatResult1;
	$resultado[1] = $repeatResult2;
	return $resultado;
}

//calcuate year over year
function repeatYearOverYear($diffDates, $dateStart, $dateEnd)
{
	$repeatResult1 = repeatPeriodsAgo('0', $diffDates, $dateStart, $dateEnd,'0');
	$dateStart_anterior = repeatRestYears(1, $dateStart);
	$dateEnd_anterior = repeatRestYears(1, $dateEnd);
	$repeatResult2 = repeatPeriodsAgo('0', $diffDates, $dateStart_anterior, $dateEnd_anterior,'0');
	return repeatResfinal($repeatResult1, $repeatResult2);
}

//rest years
function repeatRestYears($cant, $date)
{
	$years="-" . $cant . " year";
	$date = strtotime ( $years , strtotime ( $date ) ) ;
	$date = date ( 'Y-m-d' , $date );
	return $date;
}

?>
