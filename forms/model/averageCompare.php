<?php

$avgArrayDates=avgDates();
$avgDate1=$avgArrayDates[0];
$avgDate2=$avgArrayDates[1];
$avgDiffDates=(int)avgDays($avgDate1, $avgDate2);

if($_SESSION['periods'] == 0)
{
	$avgRes1 = avgPeriodsAgo('0',$avgDiffDates, $avgDate1, $avgDate2, '0');
	$avgRes2 = avgPeriodsAgo('1',$avgDiffDates, $avgDate1, $avgDate2, avgCalculateDaysAgo($avgDiffDates));
	avgResFinal($avgRes1, $avgRes2);
}else{
	if($_SESSION['periods'] == 1)
	{
		avgGetAvarageByPeriod('4', $avgDiffDates, $avgDate1, $avgDate2);	
	}else{
		if($_SESSION['periods'] == 2)
		{
			avgYearOverYear($avgDiffDates, $avgDate1, $avgDate2);	
		}
	}	
}

//dates initial an final selected
function avgDates()
{
	if(empty($_SESSION))
		session_start();
	$become=$_SESSION['f'];
	$divide=explode('/', $become);
	$mounth=$divide[0];
	$dia1=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $dia1;
	unset($divide); unset($year);
	$d1=date("Y-m-d", strtotime($date));
	$become=$_SESSION['f2'];
	$divide=explode('/', $become);
	$mounth=$divide[0];
	$day=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $day;
	unset($divide); unset($mounth); unset($day); unset($year);
	$d2=date("Y-m-d", strtotime($date));

	$range[0]=$d1;
	$range[1]=$d2;
	
	return $range;
}

//time between date initial and date finaly
function avgDays($avgDate1, $avgDate2)
{
	$days	= (strtotime($avgDate2)-strtotime($avgDate1))/86400;
	$days 	= abs($days); $days = floor($days);		
	return $days;
}

//get avarage
function avgGetAvarage($dateStart,$dateEnd)
{
	include("../model/conexion.php");
	$avarage = 0;
	
	$idStore=$_SESSION['loc_id'];
	$sql="SELECT avg(time_to_sec(r.acc_time)/60)
		  FROM zones z, sensors s, radacct r
		  WHERE z.store_code = $idStore
		  AND z.zone_code = s.zone_code
		  AND s.sensor_code = r.sensor_code
		  AND r.acc_stop_time BETWEEN '$dateStart' and '$dateEnd'";
	$result=mysql_query($sql);
	
	while($reg=mysql_fetch_array($result))
		$avarage = $reg[0];
	
	mysql_close($conexion);
	if($avarage == null)
		$avarage = 0;
	return $avarage;
}

//get periods Ago
function avgPeriodsAgo($cantPeriods,$diffDates, $date1, $date2, $daysAgo)
{
	$dateStart = null;
	$dateEnd = null;	
	if($cantPeriods > 0)
	{ 
		$day="-" . $daysAgo . " day";
		$dateStart = strtotime ( $day , strtotime ( $date1 ) ) ;
		$dateStart = date ( 'Y-m-d' , $dateStart );
		$daysToSum = "+" . $diffDates . " day";
		$dateEnd = strtotime ( $daysToSum , strtotime ( $dateStart ) );
		$dateEnd = date ( 'Y-m-d' , $dateEnd );
	}else{
		$dateStart = $date1;
		$dateEnd = $date2;
	}
	//=========== esto es para tomar en cuenta la fecha hasta 
	$daysToSum = "+ 1 day";
		$dateEnd = strtotime ( $daysToSum , strtotime ( $dateEnd ) );
		$dateEnd = date ( 'Y-m-d' , $dateEnd );
	///=======
	return avgGetAvarage($dateStart,$dateEnd);
}

//calculate days ago
function avgCalculateDaysAgo($diffDates)
{
	$daysAgo = 0;
	if($diffDates>6){
		$daysAgo = (((round($diffDates/7))+1)*7);
	}else{
		$daysAgo = 7;
	}
	return $daysAgo;
}

//calculate avarage by period
function avgGetAvarageByPeriod($periods, $diffDates, $date1, $date2)
{
	$daysAgo = avgCalculateDaysAgo($diffDates);
	$opResult1 = avgPeriodsAgo('0',$diffDates, $date1, $date2, '0');
	$total =0;
	$opRes = 0;
	
	if($periods > 1)
	{
		for ($i=1; $i <= $periods; $i++) { 
			$total += avgPeriodsAgo($periods, $diffDates, $date1, $date2,$daysAgo*$i);
		}
		$opRes2 = $total/$periods; 
	}else{
		$opRes2=avgPeriodsAgo($periods, $diffDates, $date1, $date2, $daysAgo);
	}
	avgResFinal($opResult1, $opRes2);
}

// get result final
function avgResFinal($avgRes1, $avgResult2)
{
	echo '<div id="yesterday">Selected Period</div>
			<div id="datos">'.round($avgRes1,1).' min </div>
			<div id="previus">vs. Previous Period</div>
			<div id="flechita">';
	if($avgRes1>$avgResult2)//ha bajado el porcentaje
	{
		$aux = $avgRes1 - $avgResult2; 
		if($avgRes1>0)	
			$res=(($aux/$avgRes1)*100);
		else
			$res=0;
		$n=number_format($res, 1, '.', '');
		echo $n . "%  ";			
		echo "<img src='../img/up.jpg' border='0'>";
	}
	else
	{
		$aux = $avgResult2 - $avgRes1;
		if($avgResult2>0)	
			$res=(($aux/$avgResult2)*100);
		else
			$res=0;
		$n=number_format($res, 1, '.', '');
		echo $n . "%  ";
		echo "<img src='../img/down.png' border='0'>";	
	}
	echo '</div>';
}

//calculate year over year
function avgYearOverYear($diffDates, $dateIni, $dateEnd)
{
	$avgResult1 = avgPeriodsAgo('0', $diffDates, $dateIni, $dateEnd,'0');
	$dateIniLast = avgRestYears(1, $dateIni);
	$dateEndLast = avgRestYears(1, $dateEnd);
	$avgResult2 = avgPeriodsAgo('0', $diffDates, $dateIniLast, $dateEndLast,'0');
	avgResFinal($avgResult1, $avgResult2);
}

//rest year to date
function avgRestYears($cant, $date)
{
	$years="-" . $cant . " year";
	$date = strtotime ( $years , strtotime ( $date ) ) ;
	$date = date ( 'Y-m-d' , $date );
	return $date;
}

?>
