<?php

$function = $_POST['function'];

if($function == "getSensor")
{
	getSensor();
}else{
	if($function == "deleteSensor")
	{
		deleteSensor($_POST['idSensor']);
	}else{
		if($function == "insertSensorDefault")
		{
			insertSensorDefault($_POST['idZone'], $_POST['mac']);
		}else{
			if($function == "existMac")
			{
				existMac($_POST['mac']);
			}else{
				$mac=$_POST['mac'];
				$name=$_POST['name'];
				$model=$_POST['model'];
				$type=$_POST['type'];
				$ip=$_POST['ip'];
				$sensorReadSampleTime=$_POST['sensorReadSampleTime'];
	            $sensorProgramStartTime=$_POST['sensorProgramStartTime']; 
	            $sensorProgramEndTime=$_POST['sensorProgramEndTime'];
	            $sensortimeMaxToSismiss=$_POST['sensortimeMaxToSismiss'];
	            $sensorMaxLogFileTime=$_POST['sensorMaxLogFileTime'];
	            $sensorRaspberryCsvPath=$_POST['sensorRaspberryCsvPath']; 
	            $sensorGeneratedCsvPath=$_POST['sensorGeneratedCsvPath'];
	            $sensorLogPath=$_POST['sensorLogPath'];
	            $sensorExternalProgramPath=$_POST['sensorExternalProgramPath'];
	            $sensorCopyCsvFilesPath=$_POST['sensorCopyCsvFilesPath'];
	            $sensorDismissedFilePath=$_POST['sensorDismissedFilePath']; 
	            $sensorSignalUpperLimit=$_POST['sensorSignalUpperLimit'];
	            $sensorTprima=$_POST['sensorTprima'];
	            $sensorVisitorFrame=$_POST['sensorVisitorFrame'];
	            $sensorTimeTobeVisitor=$_POST['sensorTimeTobeVisitor'];

				if($function == "updateSensor")
				{
					$idSensor = $_POST['idSensor']; 
					updateSensor($mac, $name, $model, $type, $ip, $sensorReadSampleTime, $sensorProgramStartTime, $sensorProgramEndTime, $sensortimeMaxToSismiss, $sensorMaxLogFileTime, $sensorRaspberryCsvPath, $sensorGeneratedCsvPath, $sensorLogPath, $sensorExternalProgramPath, $sensorCopyCsvFilesPath, $sensorDismissedFilePath, $sensorSignalUpperLimit, $sensorTprima, $sensorVisitorFrame, $sensorTimeTobeVisitor, $idSensor);
				}else{	
					$zone=$_POST['zone'];
					include_once("conexion.php");

					$sql= "INSERT INTO sensors (sensor_code, model, name, type, ip, read_sample_time, program_start_time, program_end_time, 
												t_max_to_dismiss, max_log_file_time, raspberry_csv_path, generated_csv_path, log_path, 
												external_program_path, copy_csv_files_path, dismissed_file_path, signal_upper_limit, t_prima,
												visitor_frame, time_to_be_visitor, zone_code) 
										VALUES ('$mac', '$model', '$name', '$type', '$ip', '$sensorReadSampleTime',
												'$sensorProgramStartTime', '$sensorProgramEndTime', '$sensortimeMaxToSismiss',
												'$sensorMaxLogFileTime', '$sensorRaspberryCsvPath', '$sensorGeneratedCsvPath',
												'$sensorLogPath', '$sensorExternalProgramPath', '$sensorCopyCsvFilesPath',
												'$sensorDismissedFilePath', '$sensorSignalUpperLimit', '$sensorTprima','$sensorVisitorFrame',
												'$sensorTimeTobeVisitor', $zone)";
					
					$result=mysql_query($sql);
					mysql_close($conexion);
					echo $sql;
				}
			}	
		}		
	}	
	
}

//delete sensor in BD
function deleteSensor($idSensor)
{
	include_once("conexion.php");
	$sql= "DELETE FROM sensors	WHERE sensor_code = '$idSensor'";
	$result=mysql_query($sql);
	mysql_close($conexion);
}

//update sensor in BD
function updateSensor($mac, $name, $model, $type, $ip, $sensorReadSampleTime, $sensorProgramStartTime, $sensorProgramEndTime, $sensortimeMaxToSismiss, $sensorMaxLogFileTime, $sensorRaspberryCsvPath, $sensorGeneratedCsvPath, $sensorLogPath, $sensorExternalProgramPath, $sensorCopyCsvFilesPath, $sensorDismissedFilePath, $sensorSignalUpperLimit, $sensorTprima, $sensorVisitorFrame, $sensorTimeTobeVisitor, $idSensor)
{
	include_once("conexion.php");
	$sql = "UPDATE sensors SET sensor_code = '$mac', name='$name', model='model', type ='$type',ip='$ip',
			read_sample_time = '$sensorReadSampleTime', program_start_time ='$sensorProgramStartTime', 
			program_end_time= '$sensorProgramEndTime', t_max_to_dismiss='$sensortimeMaxToSismiss',max_log_file_time='$sensorMaxLogFileTime', 
			raspberry_csv_path='$sensorRaspberryCsvPath',generated_csv_path='$sensorGeneratedCsvPath',
			log_path='$sensorLogPath', external_program_path ='$sensorExternalProgramPath', copy_csv_files_path = '$sensorCopyCsvFilesPath',
			dismissed_file_path='$sensorDismissedFilePath', signal_upper_limit='$sensorSignalUpperLimit',
		    t_prima='$sensorTprima',visitor_frame='$sensorVisitorFrame', time_to_be_visitor='$sensorTimeTobeVisitor' 
			WHERE sensor_code = '$idSensor'";
	$result=mysql_query($sql);
	mysql_close($conexion);
	echo json_encode("se actualizo bn $sql");
}

//get sensor
function getSensor()
{
	$idSensor = $_POST['idSensor'];
	include_once("conexion.php");
	$sensor = null;
	if($idSensor == 'default'){
		$sensor = getSensorDefault();
	}else{
		$sql= "SELECT * FROM sensors WHERE sensor_code = '$idSensor'";
		$result = mysql_query($sql);

		if($row = mysql_fetch_array($result))
		{
			$sensor[0] = $row[0];
			$sensor[1] = $row[1];
			$sensor[2] = $row[2];
			$sensor[3] = $row[3];
			$sensor[4] = $row[4];
			$sensor[5] = $row[5];
			$sensor[6] = $row[6];
			$sensor[7] = $row[7];
			$sensor[8] = $row[8];
			$sensor[9] = $row[9];
			$sensor[10] = $row[10];
			$sensor[11] = $row[11];
			$sensor[12] = $row[12];
			$sensor[13] = $row[13];
			$sensor[14] = $row[14];
			$sensor[15] = $row[15];
			$sensor[16] = $row[16];
			$sensor[17] = $row[17];
			$sensor[18] = $row[18];
			$sensor[19] = $row[19];
			$sensor[20] = $row[20];
		}
	}	
	
	mysql_close($conexion);
	echo json_encode($sensor);  		
}

//get sensor default
function getSensorDefault()
{
	$sensorReadSampleTime = selctValueParameter('read_sample_time');
	$sensorProgramStartTime = selctValueParameter('program_start_time');
	$sensorProgramEndTime = selctValueParameter('program_end_time');
	$sensortimeMaxToSismiss = selctValueParameter('t_max_to_dismiss');
	$sensorMaxLogFileTime = selctValueParameter('max_log_file_time');
	$sensorRaspberryCsvPath = selctValueParameter('raspberry_csv_path');
	$sensorGeneratedCsvPath = selctValueParameter('generated_csv_path');
	$sensorLogPath = selctValueParameter('log_path');
	$sensorExternalProgramPath = selctValueParameter('external_program_path');
	$sensorCopyCsvFilesPath = selctValueParameter('copy_csv_files_path');
	$sensorDismissedFilePath = selctValueParameter('dismissed_file_path');
	$sensorSignalUpperLimit = selctValueParameter('signal_upper_limit');
	$sensorTprima = selctValueParameter('t_prima');
	$sensorVisitorFrame = selctValueParameter('visitor_frame');
	$sensorTimeTobeVisitor = selctValueParameter('time_to_be_visitor');
	$sensor = null;
	$sensor[0] = '';
	$sensor[1] = '';
	$sensor[2] = '';
	$sensor[3] = '';
	$sensor[4] = '';
	$sensor[5] = $sensorReadSampleTime;
	$sensor[6] = $sensorProgramStartTime;
	$sensor[7] = $sensorProgramEndTime;
	$sensor[8] = $sensortimeMaxToSismiss;
	$sensor[9] = $sensorMaxLogFileTime;
	$sensor[10] = $sensorRaspberryCsvPath;
	$sensor[11] = $sensorGeneratedCsvPath;
	$sensor[12] = $sensorLogPath;
	$sensor[13] = $sensorExternalProgramPath;
	$sensor[14] = $sensorCopyCsvFilesPath;
	$sensor[15] = $sensorDismissedFilePath;
	$sensor[16] = $sensorSignalUpperLimit;
	$sensor[17] = $sensorTprima;
	$sensor[18] = $sensorVisitorFrame;
	$sensor[19] = $sensorTimeTobeVisitor;
	return $sensor;
}

//insert dafault sensor
function insertSensorDefault($idZone, $mac)
{
	$idSensor = 0;
	include_once("conexion.php");
	$sensorReadSampleTime = selctValueParameter('read_sample_time');
	$sensorProgramStartTime = selctValueParameter('program_start_time');
	$sensorProgramEndTime = selctValueParameter('program_end_time');
	$sensortimeMaxToSismiss = selctValueParameter('t_max_to_dismiss');
	$sensorMaxLogFileTime = selctValueParameter('max_log_file_time');
	$sensorRaspberryCsvPath = selctValueParameter('raspberry_csv_path');
	$sensorGeneratedCsvPath = selctValueParameter('generated_csv_path');
	$sensorLogPath = selctValueParameter('log_path');
	$sensorExternalProgramPath = selctValueParameter('external_program_path');
	$sensorCopyCsvFilesPath = selctValueParameter('copy_csv_files_path');
	$sensorDismissedFilePath = selctValueParameter('dismissed_file_path');
	$sensorSignalUpperLimit = selctValueParameter('signal_upper_limit');
	$sensorTprima = selctValueParameter('t_prima');
	$sensorVisitorFrame = selctValueParameter('visitor_frame');
	$sensorTimeTobeVisitor = selctValueParameter('time_to_be_visitor');

	$sql= "INSERT INTO sensors (sensor_code, read_sample_time, program_start_time, program_end_time, 
								t_max_to_dismiss, max_log_file_time, raspberry_csv_path, generated_csv_path, log_path, 
								external_program_path, copy_csv_files_path, dismissed_file_path, signal_upper_limit, t_prima,
								visitor_frame, time_to_be_visitor, zone_code) 
						VALUES ('$mac', '$sensorReadSampleTime',
								'$sensorProgramStartTime', '$sensorProgramEndTime', '$sensortimeMaxToSismiss',
								'$sensorMaxLogFileTime', '$sensorRaspberryCsvPath', '$sensorGeneratedCsvPath',
								'$sensorLogPath', '$sensorExternalProgramPath', '$sensorCopyCsvFilesPath',
								'$sensorDismissedFilePath', '$sensorSignalUpperLimit', '$sensorTprima','$sensorVisitorFrame',
								'$sensorTimeTobeVisitor', $idZone)";
	$result=mysql_query($sql);
	if(!mysql_error())
		$idSensor =  $mac;

	mysql_close($conexion);			
	echo json_encode($idSensor); 
}

//exist passord?
function existMac($mac)
{
	$res = 0;
	include_once("conexion.php");
	$sql= "SELECT * from sensors where sensor_code='$mac'";
	$result=mysql_query($sql);
	if($result!=null)
		if(mysql_num_rows($result)>0)
			$res = 1;
	mysql_close($conexion);
	echo json_encode($res);
}

//select value dafault from BD
function selctValueParameter($parameter)
{
	include_once("conexion.php");
	$res = "";
	$sql= "SELECT value FROM parameters WHERE parameter = '$parameter'";
	$result=mysql_query($sql);
	if($row = mysql_fetch_array($result))
		$res = $row[0];
	return $res;
}

?>