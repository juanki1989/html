<?php
$oppLastRangeAgo=oppLastRange();
$oppLastDateInitial=$oppLastRangeAgo[0];
$oppLastDateEnd=$oppLastRangeAgo[1];
$oppLastRest=(int)oppDaysTrans($oppLastDateInitial, $oppLastDateEnd);

//calculate date Initial an finaly
function oppLastRange()
{
	if(empty($_SESSION))
	session_start();
	$transformate=$_SESSION['f'];
	$divide=explode('/', $transformate);
	$mounth=$divide[0];
	$day1=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $day1;
	unset($divide); unset($year);
	$date1=date("Y-m-d", strtotime($date));
	$transformate=$_SESSION['f2'];
	$divide=explode('/', $transformate);
	$mounth=$divide[0];
	$day=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $day;
	unset($divide); unset($mounth); unset($day); unset($year);
	$date2=date("Y-m-d", strtotime($date));

	$range[0]=$date1;
	$range[1]=$date2;
	$range[2]=$day1;
	return $range;
}

// calculate diference between date ini and date final
function oppDaysTrans($oppLastDateInitial, $oppLastDateEnd)//Tiempo en días entre la fecha final y la inicial
{
	$days	= (strtotime($oppLastDateEnd)-strtotime($oppLastDateInitial))/86400;
	$days 	= abs($days); $days = floor($days);		
	return $days;
}	

//calculate periods Ago
function oppgPeriodsAgo($cantPeriodos,$diffDates, $date1, $date2, $daysAgo)
{
	$dateStart = null;
	$dateFinal = null;	
	if($cantPeriodos > 0)
	{ 
		$days="-" . $daysAgo . " day";
		$dateStart = strtotime ( $days , strtotime ( $date1 ) ) ;
		$dateStart = date ( 'Y-m-d' , $dateStart );
		$daysToSum = "+" . $diffDates . " day";
		$dateFinal = strtotime ( $daysToSum , strtotime ( $dateStart ) );
		$dateFinal = date ( 'Y-m-d' , $dateFinal );
	}else{
		$dateStart = $date1;
		$dateFinal = $date2;
	}
	include("../../../model/conexion.php");
	$c_salida1;
	$idStore=$_SESSION['loc_id'];
	$sql="SELECT COUNT(r.mac), DATE(r.acc_stop_time)
		  FROM zones z, sensors s, radacct r
		  WHERE z.store_code = $idStore
		  AND z.zone_code = s.zone_code
		  AND s.sensor_code = r.sensor_code
		  AND r.acc_stop_time BETWEEN '$dateStart' and '$dateFinal'
		  GROUP BY DATE(r.acc_stop_time)";
	$result=mysql_query($sql);
	$i = 0;
	$masUnDia = "+ 1 day";
	$datecComparacion = $dateStart;
	$res;
	$c_salida1 = array();
	while($reg=mysql_fetch_array($result))
	{
		$c_salida1[$i][0] = $reg[0];
		$c_salida1[$i][1] = $reg[1];
		$i++;
	}
	$datecComparacion = $dateStart;
	$cant = 0;
	for($i = 0; $i < $diffDates; $i++) 
	{
		if($cant < count($c_salida1))
		{
			$date = $c_salida1[$cant][1];
			if($datecComparacion == $date)
			{
				$res[$i] = $c_salida1[$cant][0];
				$cant++;
				
			}else{
				$res[$i] = 0;	
			}	
		}else{
			$res[$i] = 0;
		}
		$datecComparacion = strtotime ( $masUnDia , strtotime ( $datecComparacion ) ) ;
		$datecComparacion = date ( 'Y-m-d' , $datecComparacion );		
	}

	mysql_close($conexion);
	return $res;
}

//calculate days ago
function oppgCalculateDaysAgo($diffDates)
{
	$daysAgo = 0;
	if($diffDates>6){
		$daysAgo = (((round($diffDates/7))+1)*7);
	}else{
		$daysAgo = 7;
	}
	return $daysAgo;
}

//get avarge by period
function oppgGetAvarageByPeriod($periods, $diffDates, $date1, $date2)
{
	$daysAgo = oppgCalculateDaysAgo($diffDates);
	if($periods > 1)
	{
		$total;
		$i = 0;
		for ($i=0; $i <= $periods-1; $i++) { 
			$total[$i]= oppgPeriodsAgo($periods, $diffDates, $date1, $date2,($daysAgo*($i+1)));
		}
		$cantArrays = count($total);
		$cantElementsArrays = count($total[0]);
		for($j = 0; $j < $cantElementsArrays ; $j++)
		{
			$avarage = 0;
			for($k = 0; $k < $cantArrays ; $k++)
			{
				$avarage += $total[$k][$j];
			}
			$avarage = $avarage/$cantArrays;
			$opResult2[$j] = $avarage; 
		}
	}else{
		$opResult2 = oppgPeriodsAgo($periods, $diffDates, $date1, $date2, $daysAgo);
	}
	
	return $opResult2;
}

//calculate year over year
function oppgYearOverYear($diffDates, $dateIni, $dateEnd)
{
	$dateIniLast = oppgRestYears(1, $dateIni);
	$dateEndLast = oppgRestYears(1, $dateEnd);
	
	$oppgResult2 = oppgPeriodsAgo('0', $diffDates,  $dateIniLast, $dateEndLast, oppgCalculateDaysAgo($diffDates));
	return $oppgResult2;
}

//rest years to some date
function oppgRestYears($cant, $date)
{
	$years="-" . $cant . " year";
	$date = strtotime ( $years , strtotime ( $date ) ) ;
	$date = date ( 'Y-m-d' , $date );
	return $date;
}

//calculate last people datas
function oppLastPeopleNew($oppLastRest, $oppLastDateInitial, $oppLastDateEnd)
{
	$res = 0;
	if($_SESSION['periods'] == 0)
	{
		$res = oppgPeriodsAgo('1', $oppLastRest, $oppLastDateInitial, $oppLastDateEnd, oppgCalculateDaysAgo($oppLastRest));
		
	}else{
		if($_SESSION['periods'] == 1)
		{
			$res = oppgGetAvarageByPeriod('4', $oppLastRest, $oppLastDateInitial, $oppLastDateEnd);
		}else{
			if($_SESSION['periods'] == 2)
			{
				$res = oppgYearOverYear($oppLastRest, $oppLastDateInitial, $oppLastDateEnd);
			}	
		}	
	}
	return $res;
}

//calculate current period people
function oppCurrentPeopleNew($oppLastRest, $oppLastDateInitial, $oppLastDateEnd)
{
	$res = oppgPeriodsAgo('0', $oppLastRest, $oppLastDateInitial, $oppLastDateEnd, oppgCalculateDaysAgo($oppLastRest));
	
	return $res;
}

?>