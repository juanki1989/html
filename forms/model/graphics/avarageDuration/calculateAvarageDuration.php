<?php 
$rangeAvg=rangeAvg();
$restAvg=(int)dTransAvg($rangeAvg[0], $rangeAvg[1]);
$dateIniAvg=$rangeAvg[0];
$dateEndAvg=$rangeAvg[1];

//calculate diff between date ini and date final
function dTransAvg($d1Avg, $d2Avg)
{
	$days	= (strtotime($d2Avg)-strtotime($d1Avg))/86400;
	$days 	= abs($days); $days = floor($days);		
	return $days;
}

//calculate date Initial an finaly
function rangeAvg()
{
	if(empty($_SESSION))
		session_start();
	$transformate=$_SESSION['f'];
	$divide=explode('/', $transformate);
	$mounth=$divide[0];
	$day1=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $day1;
	unset($divide); unset($year);
	$d1Avg=date("Y-m-d", strtotime($date));
	$transformate=$_SESSION['f2'];
	$divide=explode('/', $transformate);
	$mounth=$divide[0];
	$day=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $day;
	unset($divide); unset($mounth); unset($day); unset($year);
	$d2Avg=date("Y-m-d", strtotime($date));

	$range[0]=$d1Avg;
	$range[1]=$d2Avg;
	$range[2]=$day1;
	return $range;
}

//get avarage by week
function avgGetAvarageWeek($cantPeriods,$diffdates,$dateStart,$dateEnd, $daysAgo)
{
	include("../../../model/conexion.php");
	$result;
	if($cantPeriods > 0)
	{ 
		$days="-" . $daysAgo . " day";
		$dateStart = strtotime ( $days , strtotime ( $dateStart ) ) ;
		$dateStart = date ( 'Y-m-d' , $dateStart );
		$daysToSum = "+" . $diffdates . " day";
		$dateEnd = strtotime ( $daysToSum , strtotime ( $dateStart ) );
		$dateEnd = date ( 'Y-m-d' , $dateEnd );
		
	}else{
		$dateStart = $dateStart;
		$dateEnd = $dateEnd;
		
		$daysToSum = "+ 1 day";
		$dateEnd = strtotime ( $daysToSum , strtotime ( $dateEnd ) );
		$dateEnd = date ( 'Y-m-d' , $dateEnd );
		
	}
	$idStore=$_SESSION['loc_id'];
	for($i = 0; $i <7;$i++){
	$sql="SELECT avg(time_to_sec(r.acc_time)/60)
		  FROM zones z, sensors s, radacct r
		  WHERE z.store_code = $idStore
		  AND z.zone_code = s.zone_code
		  AND s.sensor_code = r.sensor_code
		  AND r.acc_stop_time BETWEEN '$dateStart' and '$dateEnd'
		  AND WEEKDAY(r.acc_stop_time) = $i";

	$res=mysql_query($sql);
	$reg=mysql_fetch_array($res);
	$result[$i] = $reg[0];
	$result[$i] = round($result[$i],1);
	}
	
	mysql_close($conexion);
	
	return $result;
}

//calculate days ago for graphic
function avgcalculateDaysAgoGraphic($diffDates)
{
	$daysAgo = 0;
	if($diffDates>6){
		$daysAgo = (((round($diffDates/7))+1)*7);
	}else{
		$daysAgo = 7;
	}
	return $daysAgo;
}

//get avarage by period graphic
function avgGetAvarageByPeriodGraphic($periods, $diffDates, $date1, $date2)
{
	$daysAgo = avgcalculateDaysAgoGraphic($diffDates);
	if($periods > 1)
	{
		$total;
		$i = 0;
		for ($i=0; $i <= $periods-1; $i++) { 
			$total[$i]= avgGetAvarageWeek($periods, $diffDates, $date1, $date2,$daysAgo*$i);
		}
		$cantArrays = count($total);
		$cantElementsArrays = count($total[0]);
		for($j = 0; $j < $cantElementsArrays ; $j++)
		{
			$avarage = 0;
			for($k = 0; $k < $cantArrays ; $k++)
			{
				$avarage += $total[$k][$j];
			}
			$avarage = $avarage/$cantArrays;
			$opResult2[$j] = $avarage; 
		}
		
	}else{
		$opResult2 = avgGetAvarageWeek($periods, $diffDates, $date1, $date2, $daysAgo);
	}
	return $opResult2;
}

//calculate year over year
function avggYearOverYear($diffDates, $dateIni, $dateEnd)
{
	$dateIniLast = avggRestYear(1, $dateIni);
	$dateEndLast = avggRestYear(1, $dateEnd);
	$avggResult2 = avgGetAvarageWeek('0', $diffDates,  $dateIniLast, $dateEndLast, avgcalculateDaysAgoGraphic($diffDates));
	return $avggResult2;
}

//rest years
function avggRestYear($cantidad, $date)
{
	$years="-" . $cantidad . " year";
	$date = strtotime ( $years , strtotime ( $date ) ) ;
	$date = date ( 'Y-m-d' , $date );
	return $date;
}

//get avarage peiods graphic
function avgGetAvaragePeriodsGraphic($oppLastRest, $dateStart, $dateEnd)
{
	$res = 0;
	if($_SESSION['periods'] == 0)
	{
		$res = avgGetAvarageWeek('1', $oppLastRest, $dateStart, $dateEnd, avgcalculateDaysAgoGraphic($oppLastRest));
	}else{
		if($_SESSION['periods'] == 1)
		{
			$res = avgGetAvarageByPeriodGraphic('4', $oppLastRest, $dateStart, $dateEnd);
		}else{
			if($_SESSION['periods'] == 2)
			{
				$res = avggYearOverYear($oppLastRest, $dateStart, $dateEnd);
			}
		}	
	}
	return $res;
}

//get avarage week graphic new
function avgGetAvarageweekGraphicNew($oppLastRest, $oppLastDateInitial, $oppLastDateEnd)
{
	$res = avgGetAvarageWeek('0', $oppLastRest, $oppLastDateInitial, $oppLastDateEnd, avgcalculateDaysAgoGraphic($oppLastRest));
	
	return $res;
}

?>



