<?php
$convLastRange=convLastRange();
$convLastDateInitial=$convLastRange[0];
$convLastDateEnd=$convLastRange[1];
$convLastRest=(int)convLastDaysTrans($convLastDateInitial, $convLastDateEnd);

//calculate date Initial an finaly
function convLastRange()
{
	if(empty($_SESSION))
		session_start();
	$transformate=$_SESSION['f'];
	$divide=explode('/', $transformate);
	$mounth=$divide[0];
	$day1=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $day1;
	unset($divide); unset($year);
	$date1=date("Y-m-d", strtotime($date));
	$transformate=$_SESSION['f2'];
	$divide=explode('/', $transformate);
	$mounth=$divide[0];
	$dia=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $dia;
	unset($divide); unset($mounth); unset($dia); unset($year);
	$date2=date("Y-m-d", strtotime($date));

	$range[0]=$date1;
	$range[1]=$date2;
	$range[2]=$day1;
	return $range;
}

// calculate diff days
function convLastDaysTrans($convLastDateInitial, $convLastDateEnd)//Tiempo en días entre la fecha final y la inicial
{
	$days	= (strtotime($convLastDateEnd)-strtotime($convLastDateInitial))/86400;
	$days 	= abs($days); $days = floor($days);		
	return $days;
}

//calculate periods ago
function convgPeriodsAgo($cantPeriodos,$diferenciaFechas, $date1, $date2, $daysAgo)
{
	$dateStart = null;
	$dateFinal = null;	
	if($cantPeriodos > 0)
	{ 
		$days="-" . $daysAgo . " day";
		$dateStart = strtotime ( $days , strtotime ( $date1 ) ) ;
		$dateStart = date ( 'Y-m-d' , $dateStart );
		$daysToSum = "+" . $diferenciaFechas . " day";
		$dateFinal = strtotime ( $daysToSum , strtotime ( $dateStart ) );
		$dateFinal = date ( 'Y-m-d' , $dateFinal );
	}else{
		$dateStart = $date1;
		$dateFinal = $date2;
	}
	include("../../../model/conexion.php");
	$c_out1;
	
	$idStore=$_SESSION['loc_id'];
	$sql="SELECT r.mac, DATE(r.acc_stop_time), r.visitor
		  FROM zones z, sensors s, radacct r
		  WHERE z.store_code = $idStore
		  AND z.zone_code = s.zone_code
		  AND s.sensor_code = r.sensor_code
		  AND r.acc_stop_time BETWEEN '$dateStart' and '$dateFinal'
		  ORDER BY DATE(r.acc_start_time)";
	$result=mysql_query($sql);
	$i = 0;
	$masUnDia = "+ 1 day";
	$dateComparation = $dateStart;
	$res;
	$setSelected = 0;
	$setTotal = 0;
	$firstTime = true;
	while($reg=mysql_fetch_array($result))
	{
		if($firstTime)
		{
			$dateComparation = $reg[1];
			$firstTime = false;
		}
		if($dateComparation == $reg[1])
		{
			if($reg[2] == 1)
			{
				$setSelected++;
			}	
			$setTotal++;
			
		}else{
			if($setTotal == 0)
				$c_out1[$i][0] = 0;
			else	
				$c_out1[$i][0] = ($setSelected/$setTotal)*100;
			$c_out1[$i][1] = $dateComparation;
			$i++;
			$setTotal = 0;
			$setSelected = 0;
			$dateComparation = $reg[1];
		}
		
	}
			if($setTotal == 0)
				$c_out1[$i][0] = 0;
			else
				$c_out1[$i][0] = ($setSelected/$setTotal)*100;
			$c_out1[$i][1] = $dateComparation;
			
	$dateComparation = $dateStart;
	$cant = 0;
	for($i = 0; $i < $diferenciaFechas; $i++) 	
	{
		if($cant < count($c_out1))
		{			
			$date = $c_out1[$cant][1];

			if($dateComparation == $date)
			{
				$valor = $c_out1[$cant][0];
				$res[$i] = $valor;
				$cant++;
			}else{
				$res[$i] = 0;	
			}	
		}else{
			$res[$i] = 0;
		}
		$dateComparation = strtotime ( $masUnDia , strtotime ( $dateComparation ) ) ;
		$dateComparation = date ( 'Y-m-d' , $dateComparation );		
	}
	mysql_close($conexion);
	return $res;
}

//calculate days ago
function convgCalculateDaysAgo($diffDates)
{
	$daysAgo = 0;
	if($diffDates>6){
		$daysAgo = (((round($diffDates/7))+1)*7);
	}else{
		$daysAgo = 7;
	}
	return $daysAgo;
}

//get avarage by period
function convgGetAvarageByPeriod($periods, $diffDates, $date1, $date2)
{
	$daysAgo = convgCalculateDaysAgo($diffDates);
	if($periods > 1)
	{
		$total;
		$i = 0;
		for ($i=0; $i <= $periods-1; $i++) { 
			$total[$i]= convgPeriodsAgo($periods, $diffDates, $date1, $date2,$daysAgo*$i);
		}
		$cantArrays = count($total);
		$cantElementosListas = count($total[0]);
		for($j = 0; $j < $cantElementosListas ; $j++)
		{
			$avarage = 0;
			for($k = 0; $k < $cantArrays ; $k++)
			{
				$avarage += $total[$k][$j];
			}
			$avarage = $avarage/$cantArrays;
			$opResult2[$j] = $avarage; 
		}
		
	}else{
		$opResult2 = convgPeriodsAgo($periods, $diffDates, $date1, $date2, $daysAgo);
	}
	return $opResult2;
}

//calculate year over year
function convgYearOverYear($diffDates, $dateIni, $dateEnd)
{
	$dateIniLast = convgRestYears(1, $dateIni);
	$dateEndLast = convgRestYears(1, $dateEnd);
	
	$convgResult2 = convgPeriodsAgo('0', $diffDates,  $dateIniLast, $dateEndLast, convgCalculateDaysAgo($diffDates));
	return $convgResult2;
}

//rest years
function convgRestYears($cant, $date)
{
	$years="-" . $cant . " year";
	$date = strtotime ( $years , strtotime ( $date ) ) ;
	$date = date ( 'Y-m-d' , $date );
	return $date;
}

//calculate last people new
function convLastPoepleNew($oppLastRest, $oppLastDateInitial, $oppLastDateFinal)
{
	$res = 0;
	if($_SESSION['periods'] == 0)
	{
		$res = convgPeriodsAgo('1', $oppLastRest, $oppLastDateInitial, $oppLastDateFinal, convgCalculateDaysAgo($oppLastRest));
		
	}else{
		if($_SESSION['periods'] == 1)
		{
			$res = convgGetAvarageByPeriod('4', $oppLastRest, $oppLastDateInitial, $oppLastDateFinal);
		}else{
			if($_SESSION['periods'] == 2)
			{
				$res = convgYearOverYear($oppLastRest, $oppLastDateInitial, $oppLastDateFinal);
			}
		}		
	}
	return $res;
}

//calculate current people new
function convCurrentPeopleNew($oppLastRest, $oppLastDateInitial, $oppLastDateFinal)
{
	$res = convgPeriodsAgo('0', $oppLastRest, $oppLastDateInitial, $oppLastDateFinal, convgCalculateDaysAgo($oppLastRest));
	
	return $res;
}

?>



