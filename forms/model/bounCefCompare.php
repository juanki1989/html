<?php

$bounceArrayDates=bounceDates();
$bounceDate1=$bounceArrayDates[0];
$bounceDate2=$bounceArrayDates[1];
$bounceDiffDates=(int)bounceDays($bounceDate1, $bounceDate2);

if($_SESSION['periods'] == 0)
{
	$bounceResult1 = bouncePeriodsAgo('0',$bounceDiffDates, $bounceDate1, $bounceDate2, '0');
	$bounceResult2 = bouncePeriodsAgo('1',$bounceDiffDates, $bounceDate1, $bounceDate2, bounceCalculateDaysAgo($bounceDiffDates));
	bounceResfinal($bounceResult1, $bounceResult2);
}else{
	if($_SESSION['periods'] == 1)
	{
		bounceGetAvarageByPeriods('4', $bounceDiffDates, $bounceDate1, $bounceDate2);	
	}else{
		if($_SESSION['periods'] == 2)
		{
			bounceYearOverYear($bounceDiffDates, $bounceDate1, $bounceDate2);	
		}
	}	
}

//date initial and date final selected
function bounceDates()
{
	if(empty($_SESSION))
		session_start();
	$become=$_SESSION['f'];
	$divide=explode('/', $become);
	$mounth=$divide[0];
	$day1=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $day1;
	unset($divide); unset($year);
	$d1=date("Y-m-d", strtotime($date));
	$become=$_SESSION['f2'];
	$divide=explode('/', $become);
	$mounth=$divide[0];
	$dia=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $dia;
	unset($divide); unset($mounth); unset($dia); unset($year);
	$d2=date("Y-m-d", strtotime($date));

	$range[0]=$d1;
	$range[1]=$d2;
	$range[2]=$day1;
	return $range;
}

//time between date inicil and date final
function bounceDays($bounceDate1, $bounceDate2)//Tiempo en días entre la date final y la inicial
{
	$years	= (strtotime($bounceDate2)-strtotime($bounceDate1))/86400;
	$years 	= abs($years); $years = floor($years);		
	return $years;
}

//calculate get avarage
function bounceGetAvarage($dateStart,$dateEnd)
{
	include("../model/conexion.php");
	$avarage = 0;
	$total = 0;
	$resFinal = 0;
	
	$idStore=$_SESSION['loc_id'];
		$sql="SELECT COUNT( r.mac ) 
		  FROM zones z, sensors s, radacct r
		  WHERE z.store_code = $idStore
		  AND z.zone_code = s.zone_code
		  AND s.sensor_code = r.sensor_code
		  AND r.acc_stop_time BETWEEN '$dateStart' and '$dateEnd'
		  AND (time_to_sec(r.acc_time)/60)<5";
	$result=mysql_query($sql);
	
	while($reg=mysql_fetch_array($result))
	{
		$avarage = $reg[0];
	}

	$sql2="SELECT COUNT( r.mac ) 
		  FROM zones z, sensors s, radacct r
		  WHERE z.store_code = $idStore
		  AND z.zone_code = s.zone_code
		  AND s.sensor_code = r.sensor_code
		  AND r.acc_stop_time BETWEEN '$dateStart' and '$dateEnd'";
	$result2=mysql_query($sql2);
	
	while($reg2=mysql_fetch_array($result2))
	{
		$total = $reg2[0];
	}

	mysql_close($conexion);
	if($avarage == null)
		$avarage = 0;
	if($total == null)
		$total = 0;
	if($total == 0)
		$resFinal = 0;
	else	
		$resFinal = round(($avarage/$total)*100);
	
	return $resFinal;
}

//calculate avarage with perdios ago
function bouncePeriodsAgo($cantPeriods,$diffDates, $date1, $date2, $daysAgo)
{
	$dateStart = null;
	$dateEnd = null;	
	if($cantPeriods > 0)
	{ 
		$years="-" . $daysAgo . " day";
		$dateStart = strtotime ( $years , strtotime ( $date1 ) ) ;
		$dateStart = date ( 'Y-m-d' , $dateStart );
		$yearsToSum = "+" . $diffDates . " day";
		$dateEnd = strtotime ( $yearsToSum , strtotime ( $dateStart ) );
		$dateEnd = date ( 'Y-m-d' , $dateEnd );
	}else{
		$dateStart = $date1;
		$dateEnd = $date2;
	}
	//=========== esto es para tomar en cuenta la date hasta 
	$yearsToSum = "+ 1 day";
		$dateEnd = strtotime ( $yearsToSum , strtotime ( $dateEnd ) );
		$dateEnd = date ( 'Y-m-d' , $dateEnd );
	return bounceGetAvarage($dateStart,$dateEnd);
}

//calculate days ago
function bounceCalculateDaysAgo($diffDates)
{
	$daysAgo = 0;
	if($diffDates>6){
		$daysAgo = (((round($diffDates/7))+1)*7);
	}else{
		$daysAgo = 7;
	}
	return $daysAgo;
}

//get avarage by periods
function bounceGetAvarageByPeriods($periods, $diffDates, $date1, $date2)
{
	$daysAgo = bounceCalculateDaysAgo($diffDates);
	$opResult1 = bouncePeriodsAgo('0',$diffDates, $date1, $date2, '0');
	$total =0;
	$opResult2 = 0;
	if($periods > 1)
	{
		for ($i=1; $i <= $periods; $i++) { 
			$total += bouncePeriodsAgo($periods, $diffDates, $date1, $date2,$daysAgo*$i);
		}
		$opResult2 = $total/$periods; 
	}else{		
		$opResult2 = bouncePeriodsAgo($periods, $diffDates, $date1, $date2, $daysAgo);
	}
	bounceResfinal($opResult1, $opResult2);
}

//draw res final
function bounceResfinal($bounceResult1, $bounceResult2)
{
	echo '<div id="yesterday">Selected Period</div>
			<div id="datos">'.round($bounceResult1).'%</div>
			<div id="previus">vs. Previous Period</div>
			<div id="flechita">';
	if($bounceResult1>$bounceResult2)//ha bajado el porcentaje
	{
		$aux = $bounceResult1 - $bounceResult2;
		if($bounceResult1>0)	
			$res=(($aux/$bounceResult1)*100);
		else
			$res=0;
		$n=number_format($res, 1, '.', '');
		echo $n . "%  ";			
		echo "<img src='../img/up.jpg' border='0'>";
	}
	else
	{
		$aux = $bounceResult2 - $bounceResult1;
		if($bounceResult2>0)	
			$res=(($aux/$bounceResult2)*100);
		else
			$res=0;
		$n=number_format($res, 1, '.', '');
		echo $n . "%  ";
		echo "<img src='../img/down.png' border='0'>";	
	}
	echo '</div>';
}

//calcualte year over year
function bounceYearOverYear($diffDates, $dateStart, $dateEnd)
{
	$bounceResult1 = bouncePeriodsAgo('0', $diffDates, $dateStart, $dateEnd,'0');
	$dateStartLast = bounceRestYears(1, $dateStart);
	$dateEndLast = bounceRestYears(1, $dateEnd);
	
	$bounceResult2 = bouncePeriodsAgo('0', $diffDates, $dateStartLast, $dateEndLast,'0');
	bounceResfinal($bounceResult1, $bounceResult2);
}

//rest years
function bounceRestYears($cant, $date)
{
	$years="-" . $cant . " year";
	$date = strtotime ( $years , strtotime ( $date ) ) ;
	$date = date ( 'Y-m-d' , $date );
	return $date;
}
	
?>

	
