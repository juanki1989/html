<?php

$function = $_POST['function'];
if($function == "getIdSessionCustomer")	
{
	getIdSessionCustomer();	
}else{
	if($function == "getCustomers")	
	{
		getCustomers();	
	}else{
		if($function == "getCustomer")
		{
			getCustomer($_POST['idCustomer']);
		}else{
			if($function == "deleteCustomer")
			{
				deleteCustomer($_POST['idCustomer']);
			}else{
				if($function == "existPassword")
				{
					existPassword($_POST['idCustomer'], $_POST['oldPassword']);
				}else{
					if($function == "changePassword")
					{
						changePassword($_POST['idCustomer'], $_POST['newPassword']);
					}else{
						if($function == "validCode")
						{
							validCode($_POST['code'], $_POST['email']);
						}else{
							if($function == "changePasswordWhitEmail")
							{
								changePasswordWhitEmail($_POST['email'], $_POST['newPassword']);
							}else{
								if($function == "insertResetCode")
								{
									insertResetCode($_POST['code'], $_POST['email']);
								}else{
									$name=$_POST['name'];
									$dir=$_POST['address'];
									$mail=$_POST['email'];
									$pass=$_POST['password'];
									$tel=$_POST['telephone'];
									$salt="43E$24.Csr9?wc3$2dfSR.=dc$2s72#@";
									if($function == "updateCustomer")
									{
										//update customer
										$idCustomer=$_POST['idCustomer'];
										updateCustomer($name, $dir, $mail, $pass, $tel, $salt, $idCustomer);
									}else{
										//insert customer
										insertCustomer($name, $dir, $mail, $pass, $tel, $salt);
									}
								}
							}
						}
					}
				}
			}	
		}
	}	
}

//get id session customer
function getIdSessionCustomer()
{
	session_start();
	echo $_SESSION['usuario'];
}	

//insert customer in BD
function insertCustomer($name, $dir, $mail, $pass, $tel, $salt)
{
	$pass_hash=crypt($pass, $salt);

	include_once("conexion.php");
	$sql= "INSERT INTO customer (address, responsible, telephone, mail, password) VALUES 
	('$dir', '$name', '$tel', '$mail', '$pass_hash')";
	$result=mysql_query($sql);
	echo "termino".$sql;
	mysql_close($conexion);
}

//update customer
function updateCustomer($name, $dir, $mail, $pass, $tel, $salt, $idCustomer)
{
    $pass_hash=crypt($pass, $salt);
	include_once("conexion.php");
	if($pass=="")
		$sql= "UPDATE customer SET address = '$dir', responsible = '$name', telephone = '$tel', mail = '$mail' WHERE customer_code = $idCustomer";
	else{
		$sql= "UPDATE customer SET address = '$dir', responsible = '$name', telephone = '$tel', mail = '$mail', password = '$pass_hash' WHERE customer_code = $idCustomer";
	}		
	$result=mysql_query($sql);
	if(!empty($error = mysql_error()))
		echo 'mysql error: '.$error;
	
	echo "termino".$sql;
	mysql_close($conexion);
}

//get customers array
function getCustomers()
{
	$array = array();
	include_once("conexion.php");
	$sql= "SELECT * FROM customer";
	
	$result=mysql_query($sql);
	while($row = mysql_fetch_array($result))
	{
		$customer[0] = $row[0];
		$customer[1] = $row[1];
		$customer[2] = $row[2];
		$customer[3] = $row[3];
		$customer[4] = $row[4];
		array_push($array, $customer);
	}
	mysql_close($conexion);
	echo json_encode($array); 
}

//get customer
function getCustomer($idCustomer)
{
	include_once("conexion.php");
	$sql= "SELECT * FROM customer WHERE customer_code = $idCustomer";
	
	$result=mysql_query($sql);
	if($row = mysql_fetch_array($result))
	{
		$customer[0] = $row[0];
		$customer[1] = $row[1];
		$customer[2] = $row[2];
		$customer[3] = $row[3];
		$customer[4] = $row[4];
	}
	mysql_close($conexion);
	echo json_encode($customer); 
}	

//delete customer
function deleteCustomer($idCustomer)
{
	include_once("conexion.php");
	$sql= "SELECT store_code FROM stores WHERE customer_code = $idCustomer";
	$result=mysql_query($sql);
	if($result!= null)
	while($row = mysql_fetch_array($result))
		deleteStore($row[0], $conexion);
	$sql= "DELETE FROM customer WHERE customer_code = $idCustomer";
	$result=mysql_query($sql);
	mysql_close($conexion);
}

//delete store
function deleteStore($idStore, $conexion)
{
	$sql= "SELECT zone_code FROM zones WHERE store_code = $idStore";
	$result=mysql_query($sql);
	if($result!= null)
	while($row = mysql_fetch_array($result))
		deleteZone($row[0], $conexion);
	
	$sql= "DELETE FROM stores	WHERE store_code = $idStore";
	$result=mysql_query($sql);
}

//delete zone
function deleteZone($idZone, $conexion)
{
	$sql= "DELETE FROM sensors	WHERE zone_code = $idZone";
	$result=mysql_query($sql);
	$sql= "DELETE FROM zones	WHERE zone_code = $idZone";
	$result=mysql_query($sql);
}

//exist passord?
function existPassword($idCustomer, $oldPassword)
{
	$res = 0;
	include_once("conexion.php");
	$salt="43E$24.Csr9?wc3$2dfSR.=dc$2s72#@";
	$pass_hash=crypt($oldPassword, $salt);
	$sql= "SELECT * from customer where customer_code='$idCustomer' AND password = '$pass_hash'";

	$result=mysql_query($sql);
	if($result!=null)
		if(mysql_num_rows($result)>0)
			$res = 1;
	mysql_close($conexion);
	echo json_encode($res);
}

//change password
function changePassword($idCustomer, $newPassword)
{
	$res = 0;
	include_once("conexion.php");
	$salt="43E$24.Csr9?wc3$2dfSR.=dc$2s72#@";
	$pass_hash=crypt($newPassword, $salt);
	$sql= "UPDATE customer SET password = '$pass_hash' WHERE customer_code = '$idCustomer'";			
	$result=mysql_query($sql);
	if($result!=null)
		if(!mysql_error())
			$res = 1;
	mysql_close($conexion);
	echo json_encode($res);
}

//change password with email
function changePasswordWhitEmail($email, $newPassword)
{
	$res = 0;
	include_once("conexion.php");
	$salt="43E$24.Csr9?wc3$2dfSR.=dc$2s72#@";
	$pass_hash=crypt($newPassword, $salt);
	$sql= "UPDATE customer SET password = '$pass_hash' WHERE mail = '$email'";			
	
	$result=mysql_query($sql);
	if($result!=null)
		if(!mysql_error())
			$res = 1;
	mysql_close($conexion);
	echo json_encode($res);
}

//insert reset code
function insertResetCode($code, $email)
{
	$res = 0;
	include_once("conexion.php");
	$sql= "INSERT INTO code_reset_password (code, max_date_valid, email) VALUES 
		('$code', DATE_ADD(NOW(), INTERVAL +24 HOUR), '$email')";
	$result=mysql_query($sql);
	if(mysql_insert_id()!=0){
		$res = 1;
		sendMail($code, $email);
	}
		
	mysql_close($conexion);
	echo json_encode($res);
}

//valid code
function validCode($code, $email)
{
	$res = 0;
	include_once("conexion.php");
	$sql= "SELECT * FROM code_reset_password WHERE code='$code' AND email = '$email' AND NOW() < max_date_valid";
	
	$result=mysql_query($sql);
	if(mysql_num_rows($result)>0)
		$res = 1;
	mysql_close($conexion);
	echo json_encode($res);
}

//send mail
function sendMail($code, $email)
{
	$to      = $email;
	$subject = 'Reset Password BWireless';
	$message = 'Reset Password' . "\r\n" ."click here to reset your password: http://dev2.bwireless.eu/forms/pages/resetPassword.php?code=".$code."&email=".$email;
	$headers = 'From: webmaster@example.com' . "\r\n" .
	    'Reply-To: webmaster@example.com' . "\r\n" .
	    'X-Mailer: PHP/' . phpversion();

	mail($to, $subject, $message, $headers);
}

?>