<?php

$function = $_POST['function'];
include_once("conexion.php");
switch ($function) {
    case "compareCustomers":
        echo json_encode(compareCustomers($_POST['storesChecked'], $_POST['dateInit'], $_POST['dateFinal']));
        mysql_close($conexion);
        break;
    case "compareOutsideOpportunity":
        echo json_encode(compareOutsideOpportunity($_POST['storesChecked'], $_POST['dateInit'], $_POST['dateFinal']));
        mysql_close($conexion);
        break;
    case "compareOutsideConversion":
        echo json_encode(compareOutsideConversion($_POST['storesChecked'], $_POST['dateInit'], $_POST['dateFinal']));
        mysql_close($conexion);
        break;
    case "compareAvarageDuration":
        echo json_encode(compareAvarageDuration($_POST['storesChecked'], $_POST['dateInit'], $_POST['dateFinal']));
        mysql_close($conexion);
        break;
    case "compareBounceRate":
        echo json_encode(compareBounceRate($_POST['storesChecked'], $_POST['dateInit'], $_POST['dateFinal']));
        mysql_close($conexion);
        break;
    case "compareRepeatCustomers":
        echo json_encode(compareRepeatCustomers($_POST['storesChecked'], $_POST['dateInit'], $_POST['dateFinal']));
        mysql_close($conexion);
        break;
    case "compareNewCustomers":
        echo json_encode(compareNewCustomers($_POST['storesChecked'], $_POST['dateInit'], $_POST['dateFinal']));
        mysql_close($conexion);
        break;    
    default:
        echo "Your favorite color is neither red, blue, nor green!";
}

//compare customers
function compareCustomers($stores, $dateStart, $dateEnd)
{
	
	$array = array();
	for($i = 0; $i < count($stores); $i++)
	{
		$idStore = $stores[$i];
		$sql="SELECT COUNT(r.mac), st.store_name
		FROM stores st, zones z, sensors s, radacct r
		WHERE st.store_code = $idStore
		AND z.store_code = st.store_code
		AND z.zone_code = s.zone_code
		AND s.sensor_code = r.sensor_code
		AND r.acc_stop_time BETWEEN '$dateStart' and '$dateEnd'
		AND r.visitor = 1";
		$result=mysql_query($sql);
		if($row = mysql_fetch_array($result)){
			$store[0] = $row[0];
			$store[1] = $row[1];
			array_push($array, $store);
		}
	}
	
	return $array; 	
}

//compare outside opportunity
function compareOutsideOpportunity($stores, $dateStart, $dateEnd)
{
	
	$array = array();
	for($i = 0; $i < count($stores); $i++)
	{
		$idStore = $stores[$i];
		$sql="SELECT COUNT(r.mac), st.store_name
		FROM stores st, zones z, sensors s, radacct r
		WHERE st.store_code = $idStore
		AND z.store_code = st.store_code
		AND z.zone_code = s.zone_code
		AND s.sensor_code = r.sensor_code
		AND r.acc_stop_time BETWEEN '$dateStart' and '$dateEnd'";
		$result=mysql_query($sql);
		if($result!=null)
		if($row = mysql_fetch_array($result)){
			$store[0] = $row[0];
			$store[1] = $row[1];
			array_push($array, $store);
		}
	}
	
	return $array; 	
}

//compare outside opportunity
function compareOutsideConversion($stores, $dateStart, $dateEnd)
{
	$compareCustomers = compareCustomers($stores, $dateStart, $dateEnd);
	$compareOutsideOpportunity = compareOutsideOpportunity($stores, $dateStart, $dateEnd);
	$array = array();
	for($i = 0; $i < count($stores); $i++)
	{
		$res[0] = round((($compareCustomers[$i][0]/$compareOutsideOpportunity[$i][0])*100),1);
		$res[1] = $compareCustomers[$i][1];
		array_push($array, $res);
	}
	return $array;
	
}

//compare avarage Duration
function compareAvarageDuration($stores, $dateStart, $dateEnd)
{
	$array = array();
	for($i = 0; $i < count($stores); $i++)
	{
		$idStore = $stores[$i];
		$sql="SELECT avg(time_to_sec(r.acc_time)/60), st.store_name
			  FROM stores st, zones z, sensors s, radacct r
			  WHERE st.store_code = $idStore
			  AND z.store_code = st.store_code
			  AND z.zone_code = s.zone_code
			  AND s.sensor_code = r.sensor_code
			  AND r.acc_stop_time BETWEEN '$dateStart' and '$dateEnd'";
		$result=mysql_query($sql);
	
		if($reg=mysql_fetch_array($result)){
			$store[0] = round($reg[0],1);
			$store[1] = $reg[1];
		}
			
		if($store == null){
			$store[0] = 0;
			$store[1] = 0;
		}
		array_push($array, $store);
	}
	
	return $array;
}

//compare Bounce Rate
function compareBounceRate($stores, $dateStart, $dateEnd)
{
	$array = array();
	$total = compareOutsideOpportunity($stores, $dateStart, $dateEnd);

	for($i = 0; $i < count($stores); $i++)
	{
		$idStore = $stores[$i];
		
		$sql="SELECT COUNT( r.mac ), st.store_name
		  FROM stores st, zones z, sensors s, radacct r
		  WHERE st.store_code = $idStore
		  AND z.store_code = st.store_code
		  AND z.zone_code = s.zone_code
		  AND s.sensor_code = r.sensor_code
		  AND r.acc_stop_time BETWEEN '$dateStart' and '$dateEnd'
		  AND (time_to_sec(r.acc_time)/60)<5";
		$result=mysql_query($sql);
		
		if($reg=mysql_fetch_array($result))
		{
			$avarage[0] = $reg[0];
			$avarage[1] = $reg[1];
		}

		if($avarage[0] == null)
			$avarage[0] = 0;
		if($total[0] == null)
			$total[0] = 0;
		if($total[0] == 0)
			$avarage[0] = 0;
		else
			$avarage[0] = round(($avarage[0]/$total[$i][0])*100);
		array_push($array, $avarage);
	}	
	return $array; 	
}

//compare Repeat Customers
function compareRepeatCustomers($stores, $dateStart, $dateEnd)
{
	$array = array();
	for($i = 0; $i < count($stores); $i++)
	{
		$idStore = $stores[$i];
		$sql =" SELECT COUNT( MACS.mac )
				FROM (	SELECT r.mac
						FROM zones z, sensors s, radacct r
						WHERE z.store_code = $idStore
						AND z.zone_code = s.zone_code
						AND s.sensor_code = r.sensor_code
						AND r.acc_start_time BETWEEN '$dateStart'
						AND '$dateEnd'
						GROUP BY r.mac
				) AS MACS, (	SELECT r.mac
								FROM zones z, sensors s, radacct r
								WHERE z.store_code = $idStore
								AND z.zone_code = s.zone_code
								AND s.sensor_code = r.sensor_code
								AND r.acc_start_time < '$dateStart'
								GROUP BY r.mac
							) AS MACSBD
		WHERE MACS.mac = MACSBD.mac";

		$result = mysql_query($sql);
		$res = mysql_fetch_row($result);
		
		$cantRepets = $res[0];

		$sql2 = "SELECT count( MACS.mac ), MACS.store_name
				FROM (SELECT r.mac, st.store_name
						FROM stores st, zones z, sensors s, radacct r
						WHERE st.store_code = $idStore
						AND z.store_code = st.store_code
					    AND z.zone_code = s.zone_code
					    AND s.sensor_code = r.sensor_code
						GROUP BY r.mac
				) AS MACS";
		
		$result2 = mysql_query($sql2);
		$res2 = mysql_fetch_row($result2);
		$cantTotalBd = $res2[0];
		$percentageRepets[0] = round((($cantRepets/$cantTotalBd)*100),2);
		$percentageRepets[1] = $res2[1];
 	array_push($array, $percentageRepets);
	}
	return $array;
}

//compare New Customers
function compareNewCustomers($stores, $dateStart, $dateEnd)
{
	$array = compareRepeatCustomers($stores, $dateStart, $dateEnd);
	for($i = 0; $i < count($stores); $i++)
	{
		$array[$i][0] = 100 - $array[$i][0];	
	}
	return $array;
}
?>
