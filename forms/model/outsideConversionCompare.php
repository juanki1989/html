<?php

$convArrayDates=convDates();
$convDate1=$convArrayDates[0];
$convDate2=$convArrayDates[1];
$convDiffDates=(int)convDays($convDate1, $convDate2);

if($_SESSION['periods'] == 0)
{
	$convResult1 = convPeriodsAgo('0', $convDiffDates, $convDate1, $convDate2, '0');
	$convResult2 = convPeriodsAgo('1', $convDiffDates, $convDate1, $convDate2, convCalculateDaysAgo($convDiffDates));
	convResFinal($convResult1, $convResult2);
	
}else{
	if($_SESSION['periods'] == 1)
		convGetAvarageByPeriod('4', $convDiffDates, $convDate1, $convDate2);	
	else
		if($_SESSION['periods'] == 2)
			convYearOverYear($convDiffDates, $convDate1, $convDate2);				
}

//calculate dates initial and finaly
function convDates()
{
	if(empty($_SESSION))
		session_start();
	$become=$_SESSION['f'];
	$divide=explode('/', $become);
	$mounth=$divide[0];
	$day1=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $day1;
	unset($divide); unset($year);
	$d1=date("Y-m-d", strtotime($date));
	$become=$_SESSION['f2'];
	$divide=explode('/', $become);
	$mounth=$divide[0];
	$dia=$divide[1];
	$year=$divide[2];
	$date= $year . "-" . $mounth . "-" . $dia;
	unset($divide); unset($mounth); unset($dia); unset($year);
	$d2=date("Y-m-d", strtotime($date));

	$range[0]=$d1;
	$range[1]=$d2;

	$range[2]=$day1;
	return $range;
}

//days between date initial an date final
function convDays($convDate1, $convDate2)//Tiempo en días entre la fecha final y la inicial
{
	$days	= (strtotime($convDate2)-strtotime($convDate1))/86400;
	$days 	= abs($days); $days = floor($days);		
	return $days;
}

//calculate periods ago
function convPeriodsAgo($cantPeriods,$diffDates, $date1, $date2, $daysAgo)
{
	$dateStart = null;
	$dateEnd = null;	
	if($cantPeriods > 0)
	{ 
		$days="-" . $daysAgo . " day";
		$dateStart = strtotime ( $days , strtotime ( $date1 ) ) ;
		$dateStart = date ( 'Y-m-d' , $dateStart );
		$daysToSum = "+" . $diffDates . " day";
		$dateEnd = strtotime ( $daysToSum , strtotime ( $dateStart ) );
		$dateEnd = date ( 'Y-m-d' , $dateEnd );
	}else{
		$dateStart = $date1;
		$dateEnd = $date2;
	}
	$daysToSum = "+ 1 day";
		$dateEnd = strtotime ( $daysToSum , strtotime ( $dateEnd ) );
		$dateEnd = date ( 'Y-m-d' , $dateEnd );
	include("../model/conexion.php");
	$outRes=0;
	$countMacs = 0;
	$idStore=$_SESSION['loc_id'];
	$sql="SELECT r.mac, r.acc_stop_time, r.av_signal, r.visitor
		  FROM zones z, sensors s, radacct r  
		  WHERE z.store_code = $idStore
		  AND z.zone_code = s.zone_code
		  AND s.sensor_code = r.sensor_code
		  AND r.acc_stop_time BETWEEN '$dateStart' and '$dateEnd'";
	$resultQuery=mysql_query($sql);
	while($res=mysql_fetch_array($resultQuery))
	{
		if($res[3] == 1)
		{
			$countMacs = $countMacs+1;
		}
		$outRes = $outRes+1;
	}
	
	if($outRes == 0)
		$outRes = 0;
	else
		$outRes= ($countMacs/$outRes)*100;

	mysql_close($conexion);
	return $outRes;
}

//draw final
function convResFinal($convResult1, $convResult2)
{
	echo '<div id="yesterday">Selected Period</div>
			<div id="datos">'.round($convResult1).'%</div>
			<div id="previus">vs. Previous Period</div>
			<div id="flechita">';
	
	if($convResult1>$convResult2)//ha bajado el porcentaje
	{
		$aux = $convResult1 - $convResult2;
		if($convResult1 > 0)
			$res=(($aux/$convResult1)*100);
		else
			$res=0;
		$n=number_format($res, 1, '.', '');
		echo $n . "%  ";			
		echo "<img src='../img/up.jpg' border='0'>";
	}
	else
	{
		$aux = $convResult2 - $convResult1;
		if($convResult2 > 0)
			$res=(($aux/$convResult2)*100);
		else
			$res=0;
		$n=number_format($res, 1, '.', '');
		echo $n . "%  ";
		echo "<img src='../img/down.png' border='0'>";	
	}
	echo '</div>';
}

//calcualte days ago
function convCalculateDaysAgo($diffDates)
{
	$daysAgo = 0;
	if($diffDates>6){
		$daysAgo = (((round($diffDates/7))+1)*7);
	}else{
		$daysAgo = 7;
	}
	return $daysAgo;
}

//calculate avarage by period
function convGetAvarageByPeriod($perdiods, $diffDates, $date1, $date2)
{
	$daysAgo = convCalculateDaysAgo($diffDates);
	$opResult1 = convPeriodsAgo('0', $diffDates, $date1, $date2, '0');
	$total =0;
	$opResult2 = 0;
	
	if($perdiods > 1)
	{
		for ($i=1; $i <= $perdiods; $i++) { 
			$total += convPeriodsAgo($perdiods, $diffDates, $date1, $date2,$daysAgo*$i);
		}
		$opResult2 = $total/$perdiods; 
	}else{
		$opResult2=convPeriodsAgo($perdiods, $diffDates, $date1, $date2, $daysAgo);
	}
	convResFinal($opResult1, $opResult2);
}

//calculate year over year
function convYearOverYear($diffDates, $dateStart, $dateEnd)
{
	$convResult1 = convPeriodsAgo('0', $diffDates, $dateStart, $dateEnd,'0');
	$dateStartLast = convRestYears(1, $dateStart);
	$dateEndLast = convRestYears(1, $dateEnd);
	
	$convResult2 = convPeriodsAgo('0', $diffDates, $dateStartLast, $dateEndLast,'0');
	convResFinal($convResult1, $convResult2);
}

//rest years
function convRestYears($cant, $date)
{
	$years="-" . $cant . " year";
	$date = strtotime ( $years , strtotime ( $date ) ) ;
	$date = date ( 'Y-m-d' , $date );
	return $date;
}


?>
