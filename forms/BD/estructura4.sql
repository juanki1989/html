SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `location` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `location` ;

-- -----------------------------------------------------
-- Table `location`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `location`.`customer` (
  `customer_code` INT NOT NULL AUTO_INCREMENT,
  `address` VARCHAR(255) NULL,
  `responsible` VARCHAR(255) NULL,
  `telephone` VARCHAR(45) NULL,
  `mail` VARCHAR(255) NULL,
  `password` VARCHAR(255) NULL,
  PRIMARY KEY (`customer_code`),
  UNIQUE INDEX `customer_code_UNIQUE` (`customer_code` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `location`.`stores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `location`.`stores` (
  `store_code` INT NOT NULL AUTO_INCREMENT,
  `store_name` VARCHAR(255) NULL,
  `address` VARCHAR(255) NULL,
  `geo_located_coordinates` VARCHAR(255) NULL,
  `responsible` VARCHAR(255) NULL,
  `telephone` VARCHAR(45) NULL,
  `mail` VARCHAR(255) NULL,
  `customer_code` INT NOT NULL,
  PRIMARY KEY (`store_code`),
  INDEX `fk_stores_customers_idx` (`customer_code` ASC),
  UNIQUE INDEX `store_code_UNIQUE` (`store_code` ASC),
  CONSTRAINT `fk_stores_customers`
    FOREIGN KEY (`customer_code`)
    REFERENCES `location`.`customer` (`customer_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `location`.`zones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `location`.`zones` (
  `zone_code` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `store_code` INT NOT NULL,
  PRIMARY KEY (`zone_code`),
  INDEX `fk_zonas_stores1_idx` (`store_code` ASC),
  CONSTRAINT `fk_zonas_stores1`
    FOREIGN KEY (`store_code`)
    REFERENCES `location`.`stores` (`store_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `location`.`sensors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `location`.`sensors` (
  `sensor_code` VARCHAR(17) NOT NULL,
  `name` VARCHAR(255) NULL,
  `model` VARCHAR(20) NULL,
  `type` VARCHAR(45) NULL,
  `ip` VARCHAR(20) NULL,
  `read_sample_time` INT NULL,
  `program_start_time` TIME NULL,
  `program_end_time` TIME NULL,
  `t_max_to_dismiss` TIME NULL,
  `max_log_file_time` INT NULL,
  `raspberry_csv_path` VARCHAR(255) NULL,
  `generated_csv_path` VARCHAR(255) NULL,
  `log_path` VARCHAR(255) NULL,
  `external_program_path` VARCHAR(255) NULL,
  `copy_csv_files_path` VARCHAR(255) NULL,
  `dismissed_file_path` VARCHAR(255) NULL,
  `signal_upper_limit` VARCHAR(3) NULL,
  `t_prima` TIME NULL,
  `visitor_frame` TIME NULL,
  `time_to_be_visitor` TIME NULL,
  `active` TINYINT(1) NULL DEFAULT 1,
  `zone_code` INT NOT NULL,
  PRIMARY KEY (`sensor_code`),
  UNIQUE INDEX `sensor_code_UNIQUE` (`sensor_code` ASC),
  INDEX `fk_sensors_zonas1_idx` (`zone_code` ASC),
  CONSTRAINT `fk_sensors_zonas1`
    FOREIGN KEY (`zone_code`)
    REFERENCES `location`.`zones` (`zone_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `location`.`radacct`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `location`.`radacct` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mac` VARCHAR(17) NULL,
  `acc_time` TIME NULL,
  `acc_starttime` DATETIME NULL,
  `acc_stop_time` DATETIME NULL,
  `av_signal` VARCHAR(3) NULL,
  `visitor` TINYINT(1) NULL,
  `sensor_code` VARCHAR(17) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `location`.`parameters`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `location`.`parameters` (
  `parameter_code` INT NOT NULL AUTO_INCREMENT,
  `parameter` VARCHAR(255) NULL,
  `value` VARCHAR(255) NULL,
  PRIMARY KEY (`parameter_code`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `location`.`admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `location`.`admin` (
  `user` INT NOT NULL AUTO_INCREMENT,
  `pass` VARCHAR(255) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`user`),
  UNIQUE INDEX `user_UNIQUE` (`user` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `location`.`code_reset_password`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `location`.`code_reset_password` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` INT NULL,
  `max_date_valid` DATETIME NULL,
  `email` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
