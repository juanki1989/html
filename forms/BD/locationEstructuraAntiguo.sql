-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 06-07-2015 a las 21:12:06
-- Versión del servidor: 5.5.43-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `location`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `user` varchar(50) NOT NULL,
  `pass` varchar(100) NOT NULL,
  PRIMARY KEY (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_code` int(6) NOT NULL AUTO_INCREMENT,
  `address` varchar(100) CHARACTER SET latin1 NOT NULL,
  `responsible` varchar(30) CHARACTER SET latin1 NOT NULL,
  `telephone` varchar(20) CHARACTER SET latin1 NOT NULL,
  `mail` varchar(50) CHARACTER SET latin1 NOT NULL,
  `password` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`customer_code`),
  UNIQUE KEY `customer_code` (`customer_code`),
  KEY `customer_code_2` (`customer_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `parameters`
--

CREATE TABLE IF NOT EXISTS `parameters` (
  `conf` int(11) NOT NULL,
  `csv_path` varchar(255) NOT NULL COMMENT 'Contiene la ruta donde se guardan los ficheros .csv, predeterminada "/var/www/location/csv/',
  `use_fich` int(5) NOT NULL COMMENT 'Tiempo minimo en milisegundos para leer el fichero después de usarlo.',
  `min_signal` int(3) NOT NULL COMMENT 'Valor minimo de la señal',
  `max_signal` int(3) NOT NULL COMMENT 'Valor máximo de la señal',
  `dismissed` varchar(255) NOT NULL COMMENT 'Contiene la ruta del fichero dismissed, con el nombre del fichero incluido.',
  `error_mail` varchar(255) NOT NULL COMMENT 'Dirección de correo electrónico donde irán las notificaciones de error ',
  `dir_php` varchar(255) NOT NULL COMMENT 'Contine el comando que ejecuta el php',
  `dbhost` varchar(255) NOT NULL COMMENT 'Dirección de la base de datos',
  `dbusuario` varchar(255) NOT NULL COMMENT 'Usuario de la base de datos',
  `dbpassword` varchar(255) NOT NULL COMMENT 'Password de la base de datos',
  `dbname` varchar(255) NOT NULL COMMENT 'Nombre de la base de datos',
  `mac_time` int(2) NOT NULL,
  PRIMARY KEY (`conf`),
  UNIQUE KEY `1` (`conf`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Guarda los parámetros de las aplicaciones que tratan los ficheros pcap y csv';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `radacct`
--

CREATE TABLE IF NOT EXISTS `radacct` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mac` varchar(30) NOT NULL,
  `spent_time` time NOT NULL,
  `accstarttime` datetime NOT NULL,
  `accstoptime` datetime NOT NULL,
  `av_signal` varchar(10) NOT NULL,
  `detector` varchar(20) NOT NULL,
  `visitor` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35468 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sensors`
--

CREATE TABLE IF NOT EXISTS `sensors` (
  `sensor_ip` varchar(20) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`sensor_ip`),
  UNIQUE KEY `sensor_ip` (`sensor_ip`),
  KEY `sensor_ip_2` (`sensor_ip`),
  KEY `sensor_ip_3` (`sensor_ip`),
  KEY `sensor_ip_4` (`sensor_ip`),
  KEY `sensor_ip_5` (`sensor_ip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stores`
--

CREATE TABLE IF NOT EXISTS `stores` (
  `store_code` int(6) NOT NULL AUTO_INCREMENT,
  `customer_code` int(6) NOT NULL,
  `sensor_ip` varchar(20) NOT NULL,
  `store_name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `geo-located-coordinates` varchar(100) NOT NULL,
  `responsible` varchar(30) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `mail` varchar(50) NOT NULL,
  PRIMARY KEY (`store_code`),
  UNIQUE KEY `sensor_ip` (`sensor_ip`,`geo-located-coordinates`),
  KEY `customer_code` (`customer_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `stores`
--
ALTER TABLE `stores`
  ADD CONSTRAINT `cuscod` FOREIGN KEY (`customer_code`) REFERENCES `customer` (`customer_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `senip` FOREIGN KEY (`sensor_ip`) REFERENCES `sensors` (`sensor_ip`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
